namespace HamerSoft.Core
{
    public abstract class Plugin : RuntimeScriptable
    {
        public abstract void SetMain(Main main);
    }
}