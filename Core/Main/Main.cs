using System;
using System.Collections.Generic;
using HamerSoft.Core.Services;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HamerSoft.Core
{
    [CreateAssetMenu(fileName = "Main", menuName = "HamerSoft/Core/Main Create", order = 0)]
    public class Main : RuntimeScriptable
    {
        protected RepositoryFactory RepositoryFactory;
        [SerializeField] private List<Plugin> plugins;
        protected Dictionary<Type, Plugin> Plugins;
        private ServiceProvider _serviceProvider;
        private static Main instance;

        public static Main Load()
        {
            Main main = Resources.Load("Main", typeof(Main)) as Main;
            if (!main)
                throw new NullReferenceException(
                    "There is no object called 'Main' in any 'Resources' folder. Make sure the object is called 'Main' and is directly in a folder called 'Resources', no subdirectory!");
            return main;
        }

        /// <summary>
        /// annotation to make sure this function is called before scene loaded, yet AFTER AWAKE!
        /// </summary>
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void FirstSceneLoaded()
        {
            instance = Load();
            SceneManager.activeSceneChanged += SceneManagerOnActiveSceneChanged;
            UnityEventService.ApplicationQuited += ApplicationQuited;
            instance.Init();
        }

        private static void ApplicationQuited()
        {
            if (!instance)
                return;
            instance.ApplicationQuit();
            instance = null;
        }

        private static void SceneManagerOnActiveSceneChanged(Scene oldScene, Scene newScene)
        {
            if (!instance)
                return;

            instance.DeInit();
            instance.Init();
        }

        public override void Init()
        {
            if (RepositoryFactory == null)
                RepositoryFactory = new RepositoryFactory();
            _serviceProvider = new ServiceProvider();
            Plugins = plugins.ToTypedDictionary();
            foreach (var plugin in Plugins)
            {
                plugin.Value.SetMain(this);
                plugin.Value.Init();
            }
        }

        public override void DeInit()
        {
            if (Plugins == null)
                return;
            foreach (KeyValuePair<Type, Plugin> pair in Plugins)
                pair.Value.DeInit();
            _serviceProvider.Dispose();
        }

        public override void ApplicationQuit()
        {
            SceneManager.activeSceneChanged -= SceneManagerOnActiveSceneChanged;
            instance = null;
            RepositoryFactory?.Dispose();
            RepositoryFactory = null;
            _serviceProvider?.Dispose();
            _serviceProvider = null;
            foreach (KeyValuePair<Type, Plugin> pair in Plugins)
                pair.Value.ApplicationQuit();
        }

        public T GetPlugin<T>() where T : Plugin
        {
            if (Plugins.ContainsKey(typeof(T)))
                return (T) Plugins[typeof(T)];
            return default;
        }

        public T GetRepository<T>() where T : IRepository
        {
            return RepositoryFactory.Get<T>();
        }
#if UNITY_EDITOR
        public static void SetPath(string path)
        {
            throw new NotImplementedException();
        }
#endif
    }
}