using System;
using System.Collections.Generic;

namespace HamerSoft.Core
{
    public abstract class AssemblyCrawledFactory<T> : IDisposable where T : class, IDisposable
    {
        protected Dictionary<Type, T> FactoryObjects;
        protected abstract Type GetTypeFilter { get; }

        public AssemblyCrawledFactory()
        {
        }

        ~AssemblyCrawledFactory()
        {
            foreach (var factoryObject in FactoryObjects)
                factoryObject.Value.Dispose();
        }

        public virtual void Dispose()
        {
            if (FactoryObjects == null)
                return;
            foreach (KeyValuePair<Type, T> pair in FactoryObjects)
            {
                T value = pair.Value;
                value.Dispose();
                value = null;
            }

            FactoryObjects = null;
        }

        protected virtual void CrawlAssembly()
        {
            FactoryObjects = new Dictionary<Type, T>();
            var typeDict = new GenericAssemblyTypeCrawler<T>(GetTypeFilter).GetTypesAsDictionary();
            foreach (KeyValuePair<Type, object> pair in typeDict)
                FactoryObjects.Add(pair.Key, CreateFactoryObject((T) pair.Value));
        }

        protected abstract T CreateFactoryObject(T value);

        public FO Get<FO>() where FO : T
        {
            if (FactoryObjects != null && FactoryObjects.ContainsKey(typeof(FO)))
                return (FO) FactoryObjects[typeof(FO)];
            return default;
        }
    }
}