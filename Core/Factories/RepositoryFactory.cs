using System;
using System.Collections.Generic;
using UnityEngine;

namespace HamerSoft.Core
{
    public class RepositoryFactory : AssemblyCrawledFactory<IRepository>
    {
        protected override Type GetTypeFilter => typeof(IRepository<>);

        public RepositoryFactory()
        {
            CrawlAssembly();
        }

        protected override IRepository CreateFactoryObject(IRepository value)
        {
            return value;
        }
    }
}