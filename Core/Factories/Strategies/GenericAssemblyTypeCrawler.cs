using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HamerSoft.Core
{
    public class GenericAssemblyTypeCrawler<T> : AssemblyTypeCrawler<T>
    {
        private readonly Type _genericType;

        public GenericAssemblyTypeCrawler(Type generic)
        {
            _genericType = generic;
        }
        
        public virtual Dictionary<Type, object> GetTypesAsDictionary()
        {
            Type classTypeArgument = typeof(T);
            var types =
                (from typeArray in (from assemblies in AppDomain.CurrentDomain.GetAssemblies()
                    where assemblies.GetName().Name != "Specs"
                    select assemblies.GetTypes())
                from t in typeArray
                where !t.FullName.ToLower().Contains(".specs.") &&
                      (_genericType != null ? t.IsGenericTypeSubClassOrDirivableFrom(_genericType): t.IsEqualOrSubClassOrAssingableFrom(classTypeArgument)) && 
                      !t.IsAbstract &&
                      !t.IsInterface
                select t).ToArray();
            return types.ToDictionary(t => t, Activator.CreateInstance);
        }
    }
}