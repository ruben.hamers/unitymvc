using System;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core.Services;

namespace HamerSoft.Core
{
    public class AssemblyTypeCrawler<T>
    {
        public virtual List<T> GetTypes()
        {
            Type[] types =
                (from typeArray in (from assemblies in AppDomain.CurrentDomain.GetAssemblies()
                    where assemblies.GetName().Name != "Specs"
                    select assemblies.GetTypes())
                from t in typeArray
                where !t.FullName.ToLower().Contains(".specs.") && 
                      t.IsEqualOrSubClassOrAssingableFrom(typeof(T)) && 
                      !t.IsAbstract &&
                      !t.IsInterface
                select t).ToArray();
            return types.Select(t => Activator.CreateInstance(t) is T ? (T) Activator.CreateInstance(t) : default).ToList();
        }
    }
}