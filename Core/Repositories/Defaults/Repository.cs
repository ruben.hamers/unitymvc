using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HamerSoft.Core;

namespace HamerSoft.Core
{
    public abstract class Repository<T> : IRepository<T> where T : IIdentifiable
    {
        private class PersistenceEntry<Tentry> : IDisposable
        {
            private Tentry _entry;
            private bool _changed, _deleted;

            internal PersistenceEntry(Tentry value)
            {
                _entry = value;
                _changed = true;
            }

            internal Tentry GetEntry()
            {
                return _entry;
            }

            internal Tentry UpdateEntry(Tentry updated)
            {
                _changed = true;
                return _entry = updated;
            }

            internal void UnFlagForChange()
            {
                _changed = false;
            }

            internal void FlagForDelete()
            {
                _changed = true;
                _deleted = true;
            }

            internal bool IsUpdated()
            {
                return _changed;
            }

            internal bool IsDeleted()
            {
                return _deleted;
            }

            public void Dispose()
            {
                _entry = default;
            }
        }

        protected IGateway<T> PersistanceGateway;
        private Dictionary<string, PersistenceEntry<T>> _dictionary;
        private Dictionary<int, IRepositoryListener<T>> _repositoryListeners;
        private Dictionary<int, IGatewayListener<T>> _gatewayListeners;

        public Repository()
        {
            _dictionary = new Dictionary<string, PersistenceEntry<T>>();
            _repositoryListeners = new Dictionary<int, IRepositoryListener<T>>();
            _gatewayListeners = new Dictionary<int, IGatewayListener<T>>();
            PersistanceGateway = CreateGateway();
        }

        protected abstract IGateway<T> CreateGateway();

        public T GetObject(string id)
        {
            T toGet = default;
            if (string.IsNullOrEmpty(id))
                return toGet;
            if (HasObjectInMemory(id))
                toGet = _dictionary[id].GetEntry();
            else if (HasObjectOnDisk(id))
            {
                Task loadTask = new Task(() => { PersistanceGateway.Load(id, loaded => toGet = loaded); });
                loadTask.RunSynchronously();
                AddObject(toGet);
            }

            return toGet;
        }

        public T UpdateObject(T toUpdate)
        {
            if (!HasObject(toUpdate))
                return default;

            _dictionary[toUpdate.Id].UpdateEntry(toUpdate);
            _repositoryListeners.SafeForeach((i, listener) =>
            {
                listener.UpdatedObject(_dictionary[toUpdate.Id].GetEntry());
            });
            return _dictionary[toUpdate.Id].GetEntry();
        }

        public List<T> GetAll()
        {
            return GetObjectsBy(o => true);
        }

        protected T GetObjectBy(Func<T, bool> filter)
        {
            foreach (KeyValuePair<string, Repository<T>.PersistenceEntry<T>> pair in _dictionary)
                if (filter.Invoke(pair.Value.GetEntry()))
                    return pair.Value.GetEntry();

            foreach (T value in PersistanceGateway.LoadLazy(_dictionary.Keys.ToList()))
            {
                if (!HasObjectInMemory(value.Id))
                {
                    _dictionary.Add(value.Id, new PersistenceEntry<T>(value));
                    _repositoryListeners.SafeForeach((i, listener) => { listener.AddedObject(value); });  
                }
                if (filter.Invoke(value))
                    return value;
            }

            return default;
        }

        protected List<T> GetObjectsBy(Func<T, bool> filter)
        {
            List<T> values = new List<T>();
            foreach (KeyValuePair<string, Repository<T>.PersistenceEntry<T>> pair in _dictionary)
                if (filter.Invoke(pair.Value.GetEntry()))
                    values.Add(pair.Value.GetEntry());

            foreach (T value in PersistanceGateway.LoadLazy(_dictionary.Keys.ToList()))
            {
                if (!HasObjectInMemory(value.Id))
                {
                    _dictionary.Add(value.Id, new PersistenceEntry<T>(value));
                    _repositoryListeners.SafeForeach((i, listener) => { listener.AddedObject(value); });  
                }
                if (filter.Invoke(value))
                    values.Add(value);
            }

            return values;
        }

        public void AddObject(T toAdd)
        {
            if (HasObject(toAdd))
                return;

            _dictionary.Add(toAdd.Id, new PersistenceEntry<T>(toAdd));
            _repositoryListeners.SafeForeach((i, listener) => { listener.AddedObject(toAdd); });
        }

        public void RemoveObject(ref T toRemove)
        {
            if (toRemove == null)
                return;
            T removed = toRemove;
            if (HasObjectInMemory(toRemove.Id))
            {
                _dictionary[toRemove.Id].FlagForDelete();
                _repositoryListeners.SafeForeach((i, listener) => { listener.RemovedObject(removed); });
            }
            else if (HasObjectOnDisk(toRemove.Id))
            {
                _gatewayListeners.SafeForeach((i, listener) => { listener.DeletedObject(removed); });
                PersistanceGateway.Delete(toRemove);
            }
        }

        public bool HasObject(T toCheck)
        {
            return toCheck != null && HasObjectById(toCheck.Id);
        }

        public bool HasObject(string id)
        {
            return !string.IsNullOrEmpty(id) && (HasObjectInMemory(id) || HasObjectOnDisk(id));
        }

        private bool HasObjectById(string id)
        {
            return !string.IsNullOrEmpty(id) && (HasObjectInMemory(id) || HasObjectOnDisk(id));
        }

        private bool HasObjectInMemory(string id)
        {
            return !string.IsNullOrEmpty(id) && _dictionary.ContainsKey(id) && !_dictionary[id].IsDeleted();
        }

        private bool HasObjectOnDisk(string id)
        {
            return PersistanceGateway.Exists(id);
        }

        public void Flush()
        {
            IEnumerable<KeyValuePair<string, PersistenceEntry<T>>> updatedEntries = GetUpdatedEntries();
            IEnumerable<KeyValuePair<string, PersistenceEntry<T>>> deletedEntries = GetDeletedEntries();
            foreach (var updated in updatedEntries)
            {
                _gatewayListeners.SafeForeach((i, listener) => { listener.PersistedObject(updated.Value.GetEntry()); });
                PersistanceGateway.Persist(updated.Value.GetEntry());
                updated.Value.UnFlagForChange();
            }

            foreach (var deleted in deletedEntries)
            {
                _gatewayListeners.SafeForeach((i, listener) => { listener.DeletedObject(deleted.Value.GetEntry()); });
                PersistanceGateway.Delete(deleted.Value.GetEntry());
                _dictionary.Remove(deleted.Key);
                deleted.Value.Dispose();
            }
        }

        public void AddListener(IPersistenceListener<T> listener)
        {
            switch (listener)
            {
                case IRepositoryListener<T> rl when !_repositoryListeners.ContainsKey(listener.GetHashCode()):
                    _repositoryListeners.Add(listener.GetHashCode(), rl);
                    break;
                case IGatewayListener<T> gl when !_gatewayListeners.ContainsKey(listener.GetHashCode()):
                    _gatewayListeners.Add(listener.GetHashCode(), gl);
                    break;
            }
        }


        public void RemoveListener(IPersistenceListener<T> listener)
        {
            switch (listener)
            {
                case IRepositoryListener<T> rl when _repositoryListeners.ContainsKey(listener.GetHashCode()):
                    _repositoryListeners.Remove(listener.GetHashCode());
                    break;
                case IGatewayListener<T> gl when _gatewayListeners.ContainsKey(listener.GetHashCode()):
                    _gatewayListeners.Remove(listener.GetHashCode());
                    break;
            }
        }

        private IEnumerable<KeyValuePair<string, PersistenceEntry<T>>> GetUpdatedEntries()
        {
            return from entry in _dictionary.ToList()
                where entry.Value.IsUpdated()
                select entry;
        }

        private IEnumerable<KeyValuePair<string, PersistenceEntry<T>>> GetDeletedEntries()
        {
            return from entry in _dictionary.ToList()
                where entry.Value.IsDeleted()
                select entry;
        }

        public void Dispose()
        {
            if (_dictionary != null)
                foreach (var persistenceEntry in _dictionary)
                    persistenceEntry.Value.Dispose();
            _repositoryListeners = null;
            _gatewayListeners = null;
            _dictionary = null;
        }
    }
}