namespace HamerSoft.Core
{
    public interface IPersistenceListener<in T> where T : IIdentifiable
    {
        
    }
    
    public interface IRepositoryListener<in T> : IPersistenceListener<T> where T : IIdentifiable
    {
        void AddedObject(T added);
        void RemovedObject(T removed);
        void UpdatedObject(T updated);
    }

    public interface IGatewayListener<in T>: IPersistenceListener<T> where T : IIdentifiable
    {
        void PersistedObject(T added);
        void DeletedObject(T removed);
    }
}