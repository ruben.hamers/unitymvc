using System;
using System.Collections.Generic;

namespace HamerSoft.Core
{
    public interface IRepository : IDisposable
    {
    }

    public interface IRepository<T> : IRepository where T : IIdentifiable
    {
        T GetObject(string id);
        T UpdateObject(T toUpdate);
        List<T> GetAll();
        void AddObject(T toAdd);
        void RemoveObject(ref T toRemove);
        bool HasObject(T toCheck);
        void Flush();
        void AddListener(IPersistenceListener<T> listener);
        void RemoveListener(IPersistenceListener<T> listener);
    }
}