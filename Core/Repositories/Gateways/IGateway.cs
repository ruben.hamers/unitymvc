using System;
using System.Collections;
using System.Collections.Generic;

namespace HamerSoft.Core
{
    public interface IGateway<T> where T : IIdentifiable
    {
        void Persist(T value);
        void Delete(T value);
        void Load(string id, Action<T> callback);
        bool Exists(string id);
        IEnumerable<T> LoadLazy(List<string> idsInMemory);
    }
}