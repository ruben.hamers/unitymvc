using System;
using UnityEngine;

namespace HamerSoft.Core.Services
{
    public class UnityEventService : IService
    {
        public static event Action ApplicationQuited;
        public static event Action<bool> ApplicationFocused, ApplicationPaused;

        private UnityEventListener _listener;

        public UnityEventService()
        {
            CreateEventListener();
        }

        private void CreateEventListener()
        {
            if(_listener)
                return;
            _listener = new GameObject("UnityEventListener").AddComponent<UnityEventListener>();
            _listener.ApplicationQuited += OnApplicationQuited;
            _listener.ApplicationPaused += ListenerOnApplicationPaused;
            _listener.ApplicationFocused += ListenerOnApplicationFocused;
            _listener.OnDestroyed += ListenerOnOnDestroyed;
        }

        private void ListenerOnApplicationFocused(bool hasFocus)
        {
            ApplicationFocused?.Invoke(hasFocus);
        }

        private void ListenerOnApplicationPaused(bool isPaused)
        {
            ApplicationPaused?.Invoke(isPaused);
        }

        private void OnApplicationQuited()
        {
            ApplicationQuited?.Invoke();
        }

        private void ListenerOnOnDestroyed(Object obj)
        {
            RemoveEvents();
        }

        private void RemoveEvents()
        {
            if (!_listener)
                return;
            _listener.ApplicationQuited -= OnApplicationQuited;
            _listener.ApplicationPaused -= ListenerOnApplicationPaused;
            _listener.ApplicationFocused -= ListenerOnApplicationFocused;
            _listener.OnDestroyed -= ListenerOnOnDestroyed;
            _listener = null;
        }

        public void Dispose()
        {
            if(_listener)
                _listener.ForceDestroyObject();
        }
    }
}