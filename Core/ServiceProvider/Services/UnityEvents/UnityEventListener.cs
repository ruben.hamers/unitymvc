using System;

namespace HamerSoft.Core.Services
{
    public class UnityEventListener : Object
    {
        public event Action ApplicationQuited;
        public event Action<bool> ApplicationFocused, ApplicationPaused;

        private void OnApplicationFocus(bool hasFocus)
        {
            ApplicationFocused?.Invoke(hasFocus);
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            ApplicationPaused?.Invoke(pauseStatus);
        }

        private void OnApplicationQuit()
        {
            ApplicationQuited?.Invoke();
        }
    }
}