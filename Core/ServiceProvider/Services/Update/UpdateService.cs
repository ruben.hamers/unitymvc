using System;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.UnityMvc;
using UnityEngine;

namespace HamerSoft.Core.Services
{
    public class UpdateService : IService
    {
        protected Dictionary<int, UpdateLoop> Loops;

        public UpdateService()
        {
            Loops = new Dictionary<int, UpdateLoop>();
        }

        public virtual int StartUpdate(Action updateMethod)
        {
            UpdateLoop loop = new GameObject("UpdateLoop").AddComponent<UpdateLoop>();
            loop.SetMethod(updateMethod);
            Loops.Add(loop.GetInstanceID(), loop);
            loop.Run();
            loop.OnDestroyed += UpdateLoopDestroyed;
            return loop.GetId;
        }

        private void UpdateLoopDestroyed(Object updateLoop)
        {
            UpdateLoop loop = updateLoop as UpdateLoop;
            loop.OnDestroyed -= UpdateLoopDestroyed;
            StopUpdate(loop.GetId);
        }

        public virtual void StartUpdate(int id)
        {
            if (!Loops.ContainsKey(id))
                return;
            Loops[id].Run();
        }

        public virtual void StopUpdate(int id)
        {
            if (!Loops.ContainsKey(id))
                return;
            UpdateLoop l = Loops[id];
            Loops.Remove(id);

            l.Stop();
            l.OnDestroyed -= UpdateLoopDestroyed;
            l.ForceDestroyObject();
        }

        public virtual void PauseUpdate(int id)
        {
            if (!Loops.ContainsKey(id))
                return;
            Loops[id].Stop();
        }

        public void Dispose()
        {
            Loops.SafeForeach((i, loop) => { StopUpdate(i); });
            Loops = new Dictionary<int, UpdateLoop>();
        }
    }
}