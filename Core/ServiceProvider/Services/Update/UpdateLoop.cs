using System;

namespace HamerSoft.Core.Services
{
    public class UpdateLoop: Object
    {
        private Action _updateMethod;
        private bool _isRunning;

        public void SetMethod(Action updateMethod)
        {
            _updateMethod = updateMethod;
        }

        public int GetId => GetInstanceID();

        public void Run()
        {
            _isRunning = true;
        }

        public void Stop()
        {
            _isRunning = false;
        }

        private void Update()
        {
            if(!_isRunning)
                return;
            _updateMethod.Invoke();
        }
    }
}