using System.Collections.Generic;
using UnityEngine.Analytics;

namespace HamerSoft.Core.Services
{
    public class AnalyticsService : IService
    {

        public AnalyticsService()
        {
            Analytics.enabled = true;
        }
        public void LogEvent(string eventName, Dictionary<string, object> eventData = null)
        {
            AnalyticsEvent.Custom(eventName, eventData);
        }

        public void Dispose()
        {
        }
    }
}