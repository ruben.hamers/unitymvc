using UnityEngine;

namespace HamerSoft.Core.Services
{
    public class CoroutineRunner : Object
    {
        protected override void Awake()
        {
            base.Awake();
            hideFlags = HideFlags.HideInHierarchy;
        }
    }
}