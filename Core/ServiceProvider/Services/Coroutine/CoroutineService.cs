using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace HamerSoft.Core.Services
{
    public class CoroutineService : IService
    {
        protected Dictionary<string, Coroutine> Coroutines;
        protected CoroutineRunner CoroutineRunner;

        public CoroutineService()
        {
            CreateRunner();
        }

        public Coroutine StartCoroutine(IEnumerator coroutine)
        {
            string id = System.Guid.NewGuid().ToString();
            Coroutine c = CoroutineRunner.StartCoroutine(WrapRoutine(coroutine, id));
            Coroutines.Add(id, c);
            return c;
        }

        private IEnumerator WrapRoutine(IEnumerator routine, string id)
        {
            while (routine.MoveNext()) yield return routine.Current;
            StopCoroutine(id);
        }


        private void CreateRunner()
        {
            CoroutineRunner = new GameObject("CoroutineRunner").AddComponent<CoroutineRunner>();
            CoroutineRunner.OnDestroyed += CoroutineRunnerOnOnDestroyed;
            Coroutines = new Dictionary<string, Coroutine>();
        }

        private void CoroutineRunnerOnOnDestroyed(Object runner)
        {
            CoroutineRunner.OnDestroyed -= CoroutineRunnerOnOnDestroyed;
            Coroutines.SafeForeach((id, coroutine) => { StopCoroutine(id); });
            Coroutines = new Dictionary<string, Coroutine>();
        }

        public void StopCoroutine(Coroutine coroutine)
        {
            if (coroutine == null)
                return;
            if (Coroutines.ContainsValue(coroutine))
                Coroutines.Remove(Coroutines.FirstOrDefault(kvp => kvp.Value == coroutine).Key);
            CoroutineRunner.StopCoroutine(coroutine);
        }

        protected void StopCoroutine(string id)
        {
            if (!Coroutines.ContainsKey(id))
                return;

            Coroutine coroutine = Coroutines[id];
            Coroutines.Remove(id);
            if (coroutine != null)
                CoroutineRunner.StopCoroutine(coroutine);
        }

        public void Dispose()
        {
            Coroutines.SafeForeach((id, coroutine) => { StopCoroutine(id); });
            DestroyCoroutineRunner();
            Coroutines = new Dictionary<string, Coroutine>();
        }

        private void DestroyCoroutineRunner()
        {
            try
            {
                CoroutineRunner.OnDestroyed -= CoroutineRunnerOnOnDestroyed;
                CoroutineRunner.ForceDestroyObject();
            }
            catch (Exception e)
            {
                // ignored since the coroutine runner is probably destroyed by scene loading.
            }
        }
    }
}