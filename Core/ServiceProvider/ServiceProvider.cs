using System;

namespace HamerSoft.Core.Services
{
    public class ServiceProvider : AssemblyCrawledFactory<IService>
    {
        private static ServiceProvider _instance;
        protected override Type GetTypeFilter => null;

        public ServiceProvider()
        {
            CrawlAssembly();
            _instance = this;
        }

        protected override IService CreateFactoryObject(IService value)
        {
            return value;
        }

        public override void Dispose()
        {
            base.Dispose();
            _instance = null;
        }

        public static T GetService<T>() where T : IService
        {
            return _instance != null ? _instance.Get<T>() : default;
        }
    }
}