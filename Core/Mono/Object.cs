using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HamerSoft.Core
{
    public class DelayedCall
    {
        private readonly Object _target;
        private readonly float _delay;
        private readonly Action _callback;
        private readonly Action<DelayedCall> _disposeCallback;
        private readonly Coroutine _coroutine;

        public DelayedCall(Object target, float delay, Action callback, Action<DelayedCall> disposeCallback)
        {
            _target = target;
            _delay = delay;
            _callback = callback;
            _disposeCallback = disposeCallback;
           _coroutine = _target.StartCoroutine(DelayRoutine());
        }
        
        private IEnumerator DelayRoutine()
        {
            yield return new WaitForSeconds(_delay);
            _callback?.Invoke();
        }

        public void Stop(bool invokeCallback = false)
        {
            if (_coroutine == null)
                return;
            _target.StopCoroutine(_coroutine);
            if (invokeCallback)
                _callback?.Invoke();

            _disposeCallback?.Invoke(this);
        }
    }

    public abstract class Object : MonoBehaviour
    {
        public event Action<Object> OnDestroyed;
        private List<DelayedCall> _delayedCalls;

        protected virtual void Awake()
        {
            _delayedCalls = new List<DelayedCall>();
        }

        protected virtual void OnEnable()
        {
        }

        protected virtual void OnDisable()
        {
        }

        protected virtual void Start()
        {
        }

        protected virtual void OnDestroy()
        {
            if (_delayedCalls != null)
            {
                for (int i = 0; i < _delayedCalls.Count; i++)
                  _delayedCalls[i].Stop();
                _delayedCalls = new List<DelayedCall>();
            }

            OnDestroyed?.Invoke(this);
        }

        protected DelayedCall InvokeDelayed(float delay, Action callback)
        {
            DelayedCall dc = new DelayedCall(this, delay, callback, RemoveCall);
            _delayedCalls.Add(dc);
            return dc;
        }

        private void RemoveCall(DelayedCall dc)
        {
            if (_delayedCalls == null || !_delayedCalls.Contains(dc))
                return;
            _delayedCalls?.Remove(dc);
        }
    }
}