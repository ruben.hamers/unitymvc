using UnityEngine;

namespace HamerSoft.Core
{
    public abstract class RuntimeScriptable : Scriptable
    {
        public abstract void Init();
        public abstract void DeInit();
        public abstract void ApplicationQuit();
    }
}