using UnityEngine;

namespace HamerSoft.Core
{
    public abstract class Scriptable : ScriptableObject
    {
        protected virtual void Awake()
        {
        }

        protected virtual void OnEnable()
        {
        }

        protected virtual void OnDisable()
        {
        }

        protected virtual void OnDestroy()
        {
        }
    }
}