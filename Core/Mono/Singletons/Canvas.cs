using UnityEngine;
using UnityEngine.UI;

namespace HamerSoft.Core.Singletons
{
    [RequireComponent(typeof(UnityEngine.Canvas))]
    public class Canvas : AbstractSingleton<Canvas>
    {
        protected UnityEngine.Canvas CanvasUnity;
        protected CanvasScaler CanvasScaler;

        protected override Canvas Instance()
        {
            return this;
        }

        protected override void Awake()
        {
            base.Awake();
            CanvasUnity = GetComponent<UnityEngine.Canvas>();
            CanvasScaler = GetComponent<CanvasScaler>();
        }

        public UnityEngine.Canvas GetCanvas => CanvasUnity;
        public CanvasScaler GetCanvasScaler => CanvasScaler;
    }
}