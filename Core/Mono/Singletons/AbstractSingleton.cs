using System;
using UnityEngine;

namespace HamerSoft.Core.Singletons
{
    public abstract class AbstractSingleton<T> : Object where T : Object
    {
        private static T _instance;

        protected override void Awake()
        {
            base.Awake();
            DestroyWhenInstanceExists();
        }

        protected bool HasInstance()
        {
            try
            {
                var checkUnitySerializeNullSting = _instance.gameObject;
                return _instance;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void SetInstance()
        {
            if (!HasInstance())
                _instance = Instance();
        }

        private void DestroyWhenInstanceExists()
        {
            if (!HasInstance())
            {
                SetInstance();   
                return;
            }
            this.ForceDestroyObject();
        }

        private void OnApplicationQuit()
        {
            _instance = null;
            this.ForceDestroyObject();
        }

        protected abstract T Instance();

        public static T GetInstance()
        {
            return _instance;
        }
    }
}