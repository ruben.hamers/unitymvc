namespace HamerSoft.Core.Singletons
{
    public class World : AbstractSingleton<World>
    {
        protected override World Instance()
        {
            return this;
        }
    }
}