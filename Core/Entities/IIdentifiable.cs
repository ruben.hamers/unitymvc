using System;

namespace HamerSoft.Core
{
    public interface IIdentifiable : IDisposable
    {
        string Id { get; }
    }
}