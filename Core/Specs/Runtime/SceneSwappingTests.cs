using System.Collections;
using System.Reflection;
using HamerSoft.Core;
using HamerSoft.Core.Services;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using Object = UnityEngine.Object;

namespace HamerSoft.Core.Specs.RunTime
{
    public class SceneSwappingTests
    {
        private const string sceneName = "SceneSwappingTests_Scene";
        private Main _main;

        public IEnumerator Setup()
        {
            yield return LoadLevel(sceneName);
            yield return new WaitForEndOfFrame();
            _main = Main.Load();
        }


        private IEnumerator LoadLevel(string levelName)
        {
            var loadSceneOperation = SceneManager.LoadSceneAsync(levelName);
            loadSceneOperation.allowSceneActivation = true;

            while (!loadSceneOperation.isDone)
                yield return null;
            yield return new WaitForSeconds(1);
        }

        [UnityTest]
        public IEnumerator When_Scene_Is_Loaded_The_Main_Calls_Init()
        {
            yield return Setup();
            yield return new WaitForEndOfFrame();
            Assert.NotNull(ServiceProvider.GetService<UpdateService>());
        }

        [UnityTest]
        public IEnumerator When_New_Scene_Is_Loaded_The_New_Main_Object_Is_Removed()
        {
            yield return Setup();
            yield return new WaitForEndOfFrame();
            UpdateService us = ServiceProvider.GetService<UpdateService>();
            yield return LoadLevel(sceneName);
            yield return new WaitForEndOfFrame();
            UpdateService usReload = ServiceProvider.GetService<UpdateService>();
            Assert.AreNotSame(us,usReload);
            yield return new WaitForEndOfFrame();
        }

        [TearDown]
        public virtual void TearDown()
        {
        }
    }
}