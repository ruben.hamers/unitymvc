using HamerSoft.Core;
using NUnit.Framework;

namespace HamerSoft.Specs.Core
{
    public class ObjectExtensionTests
    {
        [Test]
        public void Get_Argument_Returns_The_Correct_Types()
        {
            object[] arguments = {1, "hello", true};
            Assert.AreEqual(1, arguments.GetArgument<int>(0));
            Assert.AreEqual("hello", arguments.GetArgument<string>(1));
            Assert.AreEqual(true, arguments.GetArgument<bool>(2));
        }

        [Test]
        public void Get_Argument_Returns_Null_When_Types_Are_Incorrect()
        {
            object[] arguments = {1};
            Assert.AreEqual(null, arguments.GetArgument<string>(0));
        }

        [Test]
        public void Get_Argument_Returns_Null_When_Index_OutOf_Bounds()
        {
            object[] arguments = {1};
            Assert.AreEqual(0, arguments.GetArgument<int>(2));
        }

    }
}