using HamerSoft.Core;
using HamerSoft.Core.Services;
using NUnit.Framework;

namespace HamerSoft.Specs.Core
{
    /// <summary>
    /// to test the type extensions
    /// </summary>
    [TestFixture]
    public class TypeExtensionTests
    {
        private class BaseClass{}

        private class BaseGenericClass<T> where T : BaseClass
        {
        }

        private class TestView : BaseClass
        {
        }

        private class TestController : BaseGenericClass<TestView>
        {
        }

        private abstract class DelegateController<T> : BaseGenericClass<T> where T : BaseClass
        {
        }

        private class ImplementedDelegateController : DelegateController<TestView>
        {
        }


        [Test]
        public void NonGenericType_Does_Not_Inherit_Generic_Or_Derivable_From()
        {
            Assert.False(typeof(int).IsGenericTypeSubClassOrDirivableFrom(typeof(BaseGenericClass<>)));
        }

        [Test]
        public void BaseController_With_BaseView_Does_Not_Inherit_Generic_Or_Derivable_From()
        {
            Assert.False(
                typeof(BaseGenericClass<BaseClass>).IsGenericTypeSubClassOrDirivableFrom(typeof(BaseGenericClass<>)));
        }

        [Test]
        public void GenericType_Does_Inherit_Generic_Or_Derivable_From()
        {
            Assert.True(typeof(TestController).IsGenericTypeSubClassOrDirivableFrom(typeof(BaseGenericClass<>)));
        }

        [Test]
        public void Delegated_GenericType_Does_Inherit_Generic_Or_Derivable_From()
        {
            Assert.True(
                typeof(ImplementedDelegateController).IsGenericTypeSubClassOrDirivableFrom(typeof(BaseGenericClass<>)));
        }

        [Test]
        public void Object_Is_Same_OR_Subclass_Of_Object()
        {
            Assert.True(typeof(object).IsEqualOrSubClassOrAssingableFrom(typeof(object)));
        }

        [Test]
        public void Object_Is_Same_OR_Subclass_Of_String()
        {
            Assert.True(typeof(string).IsEqualOrSubClassOrAssingableFrom(typeof(object)));
        }

        [Test]
        public void Object_Is_Same_OR_Subclass_Of_TestView()
        {
            Assert.True(typeof(TestView).IsEqualOrSubClassOrAssingableFrom(typeof(object)));
        }

        [Test]
        public void Interface_Is_Same_Or_Derivable_From_Class()
        {
            Assert.True(typeof(CoroutineService).IsEqualOrSubClassOrAssingableFrom(typeof(IService)));
        }
    }
}