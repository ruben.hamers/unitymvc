using System;
using System.Collections.Generic;
using HamerSoft.Core;
using NUnit.Framework;

namespace HamerSoft.Specs.Core
{
    [TestFixture]
    public class DictionaryExtensionTests
    {
        private Dictionary<string, string> _dictionary;

        [SetUp]
        public void Setup()
        {
            _dictionary = new Dictionary<string, string>
            {
                {"a", "1"},
                {"b", "2"},
                {"c", "3"}
            };
        }

        [Test]
        public void Dictionary_Cannot_Be_Modified_Foreach_Loop()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                foreach (var kvp in _dictionary)
                {
                    _dictionary.Remove(kvp.Key);
                }
            });
        }

        [Test]
        public void Dictionary_Can_Be_Modified_During_SafeForeach_Loop()
        {
            Assert.DoesNotThrow(() => _dictionary.SafeForeach((s, s1) => { _dictionary.Remove(s); }));
        }
    }
}