using HamerSoft.Core;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.Specs.Core
{
    [TestFixture]
    public class Vector3ExtensionTests
    {
        [Test]
        public void Vector3_Is_AlmostEqual_When_Difference_Falls_Inside_The_Precision()
        {
            Assert.IsTrue(Vector3.zero.AlmostEquals(Vector3.one, 1.1f));
        }

        [Test]
        public void Vector3_Is_AlmostEqual_When_Difference_Is_The_Same_As_The_Precision()
        {
            Assert.IsTrue(Vector3.zero.AlmostEquals(Vector3.one, 1.0f));
        }

        [Test]
        public void Vector3_Is_Not_AlmostEqual_When_Difference_Is_Greater_Than_The_Precision()
        {
            Assert.IsFalse(Vector3.zero.AlmostEquals(Vector3.one, 0.9f));
        }
        
        [Test]
        public void Vector3_Is_Not_AlmostEqual_When_Difference_In_One_Axis_Is_Greater_Than_Precision()
        {
            Assert.IsFalse(Vector3.zero.AlmostEquals(Vector3.up, 0.9f));
        }
        
        [Test]
        public void Vector3_Is_Not_AlmostEqual_When_Difference_In_Two_Axis_Is_Greater_Than_Precision()
        {
            Assert.IsFalse(Vector3.zero.AlmostEquals(new Vector3(1,1,0), 0.9f));
        }
    }
}