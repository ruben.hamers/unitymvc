using HamerSoft.Core;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace HamerSoft.Specs.Core
{
    [TestFixture]
    public class ButtonExtensionTests
    {
        [Test]
        public void When_Button_Is_Null_AddListener_Does_Not_Throw_An_Exception()
        {
            Button button = null;
            Assert.DoesNotThrow(() =>
            {
                button.AddListener(() => { });
            });
        }
        
        [Test]
        public void When_Button_Is_Not_Null_AddListener_Adds_The_CallBack_To_OnClick()
        {
            Button button = new GameObject().AddComponent<Button>();
            button.AddListener(() => { Assert.True(true);});
            button.onClick.Invoke();
        }
    }
}