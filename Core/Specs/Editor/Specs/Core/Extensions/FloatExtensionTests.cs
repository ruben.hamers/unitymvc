using HamerSoft.Core;
using NUnit.Framework;

namespace HamerSoft.Specs.Core
{
    [TestFixture]
    public class FloatExtensionTests
    {
        [Test]
        public void Float_Is_AlmostEqual_When_Difference_Falls_Inside_The_Precision()
        {
            Assert.IsTrue(1f.AlmostEquals(1.5f, 0.6f));
        }

        [Test]
        public void Float_Is_AlmostEqual_When_Difference_Is_The_Same_As_The_Precision()
        {
            Assert.IsTrue(1f.AlmostEquals(1.5f, 0.5f));
        }

        [Test]
        public void Float_Is_Not_AlmostEqual_When_Difference_Is_Greater_Than_The_Precision()
        {
            Assert.IsFalse(0.9f.AlmostEquals(1.5f, 0.5f));
        }
    }
}