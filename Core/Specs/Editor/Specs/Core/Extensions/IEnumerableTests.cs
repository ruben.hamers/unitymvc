using System.Linq;
using HamerSoft.Core;
using NUnit.Framework;

namespace HamerSoft.Specs.Core
{
    /// <summary>
    /// to test the type extensions
    /// </summary>
    [TestFixture]
    public class IEnumerableTests
    {
       
        [Test]
        public void GetClean_Returns_IEnumerable_Without_Nil_Values()
        {
            object[] array = new object[] {"a", null, "b", "c", null, null, "d"};
            Assert.True(array.Sanitize().All(o=>o!=null));
        }
            
        [Test]
        public void ToTypedDictionary_Filters_Array_By_Types()
        {
            var dict = new object[] {"a", 1, true}.ToTypedDictionary();
            Assert.True(dict.Keys.Count == 3);
        }
           
    }
}