using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core.Services;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.Specs.Core
{
    public class MockCoroutineService : CoroutineService
    {
        public Dictionary<string, Coroutine> GetCoroutines => Coroutines;
    }

    public class CoroutineServiceTests : ServiceTests<MockCoroutineService>
    {
        protected override MockCoroutineService NewService()
        {
            return new MockCoroutineService();
        }

        [Test]
        public void When_Coroutine_Is_Started_It_Is_Added_To_The_Service_Cache()
        {
            IEnumerator cr()
            {
                yield return new WaitForEndOfFrame();
            }

            Coroutine c = ServiceProvider.GetService<MockCoroutineService>().StartCoroutine(cr());
            Assert.True(ServiceProvider.GetService<MockCoroutineService>().GetCoroutines.ToList()
                .Exists(kvp => kvp.Value == c));
        }

        [Test]
        public void When_Coroutine_Is_Stopped_It_Is_Removed_To_The_Service_Cache()
        {
            IEnumerator cr()
            {
                yield return new WaitForEndOfFrame();
            }

            Coroutine c = ServiceProvider.GetService<MockCoroutineService>().StartCoroutine(cr());
            ServiceProvider.GetService<MockCoroutineService>().StopCoroutine(c);
            Assert.False(ServiceProvider.GetService<MockCoroutineService>().GetCoroutines.ToList()
                .Exists(kvp => kvp.Value == c));
        }
        
    }
}