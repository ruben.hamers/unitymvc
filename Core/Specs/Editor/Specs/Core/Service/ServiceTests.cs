using HamerSoft.Core.Services;
using NUnit.Framework;

namespace HamerSoft.Specs.Core
{
    public abstract class ServiceTests<S> where S : IService
    {
        private class MockServiceProvider : ServiceProvider
        {
            public void AddService(IService service)
            {
                FactoryObjects.Add(service.GetType(), service);
            }
        }

        private MockServiceProvider _serviceProvider;
        protected abstract S NewService();

        [SetUp]
        public void Setup()
        {
            _serviceProvider = new MockServiceProvider();
            _serviceProvider.AddService(NewService());
        }

        [TearDown]
        public void TearDown()
        {
            _serviceProvider.Dispose();
        }
    }
}