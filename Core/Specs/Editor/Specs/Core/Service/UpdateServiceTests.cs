using System.Collections.Generic;
using HamerSoft.Core.Services;
using NUnit.Framework;

namespace HamerSoft.Specs.Core
{
    public class MockUpdateService : UpdateService
    {
        public Dictionary<int, UpdateLoop> GetLoops => Loops;
    }

    [TestFixture]
    public class UpdateServiceTests : ServiceTests<MockUpdateService>
    {
        protected override MockUpdateService NewService()
        {
            return new MockUpdateService();
        }

        [Test]
        public void When_Update_Is_Started_It_Is_Added_To_The_Service_Cache()
        {
            int loopId = ServiceProvider.GetService<MockUpdateService>().StartUpdate(() => { });
            Assert.True(ServiceProvider.GetService<MockUpdateService>().GetLoops.ContainsKey(loopId));
        }

        [Test]
        public void When_Update_Is_Stopped_It_Is_Removed_To_The_Service_Cache()
        {
            int loopId = ServiceProvider.GetService<MockUpdateService>().StartUpdate(() => { });
            ServiceProvider.GetService<MockUpdateService>().StopUpdate(loopId);
            Assert.False(ServiceProvider.GetService<MockUpdateService>().GetLoops.ContainsKey(loopId));
        }

        [Test]
        public void When_Update_Is_Paused_It_Is_Not_Removed_From_The_Service_Cache()
        {
            int loopId = ServiceProvider.GetService<MockUpdateService>().StartUpdate(() => { });
            ServiceProvider.GetService<MockUpdateService>().PauseUpdate(loopId);
            Assert.True(ServiceProvider.GetService<MockUpdateService>().GetLoops.ContainsKey(loopId));
        }

        [Test]
        public void When_UpdateService_Is_Disposed_All_Loops_Are_Stopped_And_Removed()
        {
            ServiceProvider.GetService<MockUpdateService>().StartUpdate(() => { });
            ServiceProvider.GetService<MockUpdateService>().StartUpdate(() => { });
            ServiceProvider.GetService<MockUpdateService>().StartUpdate(() => { });
            ServiceProvider.GetService<MockUpdateService>().Dispose();
            Assert.IsTrue(ServiceProvider.GetService<MockUpdateService>().GetLoops.Count == 0);
        }
    }
}