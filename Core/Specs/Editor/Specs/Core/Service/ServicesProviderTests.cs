using System.Reflection;
using HamerSoft.Core.Services;
using NUnit.Framework;
using Assert = UnityEngine.Assertions.Assert;

namespace HamerSoft.Specs.Core
{
    public class ServicesProviderTests
    {
        private ServiceProvider _serviceProvider;

        [SetUp]
        public void Setup()
        {
            _serviceProvider = new ServiceProvider();
        }

        [Test]
        public void When_ServiceProvider_Is_Constructed_It_Finds_All_Services()
        {
            Assert.IsTrue(_serviceProvider.Get<CoroutineService>() != null);
        }

        [Test]
        public void When_ServiceProvider_Is_Constructed_It_Allows_Access_To_GetService_Statically()
        {
            Assert.IsTrue(ServiceProvider.GetService<CoroutineService>() != null);
        }
        
        [Test]
        public void When_ServiceProvider_Is_Disposed_It_Removes_All_Service_Instances()
        {
            _serviceProvider.Dispose();
            Assert.AreEqual(null, ServiceProvider.GetService<CoroutineService>());
        }        
        [Test]
        public void When_ServiceProvider_Is_Disposed_Its_Instance_Will_Be_Set_To_Null()
        {
            _serviceProvider.Dispose();
            FieldInfo fi = typeof(ServiceProvider).GetField("_instance", BindingFlags.Static | BindingFlags.NonPublic);
            Assert.AreEqual(null, fi.GetValue(_serviceProvider));
        }

        [TearDown]
        public void TearDown()
        {
            _serviceProvider.Dispose();
        }
    }
}