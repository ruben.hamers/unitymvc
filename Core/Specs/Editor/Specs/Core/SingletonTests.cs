using HamerSoft.Core.Singletons;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.Specs.Core
{
    public class MockSingleton : AbstractSingleton<MockSingleton>
    {
        protected override MockSingleton Instance()
        {
            return this;
        }

        public virtual void CallAwake()
        {
            Awake();
        }
    }

    public class SingletonTests
    {
        private MockSingleton _singleton;

        [SetUp]
        public void Setup()
        {
            _singleton = new GameObject().AddComponent<MockSingleton>();
            _singleton.CallAwake();
        }

        [Test]
        public void When_Singleton_Is_Awoken_Its_Instance_Is_Set()
        {
            Assert.NotNull(MockSingleton.GetInstance());
        }

        [Test]
        public void When_Singleton_Is_Awoken_Twice_Only_The_First_Instance_Stays()
        {
            var instanceId = MockSingleton.GetInstance().GetInstanceID();
            var secondSingleton = new GameObject().AddComponent<MockSingleton>();
            secondSingleton.CallAwake();
            Assert.AreEqual(instanceId, MockSingleton.GetInstance().GetInstanceID());
            Assert.Catch(() =>
            {
                var go = secondSingleton.gameObject;
            });
        }

        [Test]
        public void When_Singleton_Is_Awoken_Multiple_Times_Only_The_First_Instance_Stays()
        {
            var instanceId = MockSingleton.GetInstance().GetInstanceID();
            MockSingleton secondSingleton = null;
            for (int i = 0; i < Random.Range(3, 8); i++)
            {
                secondSingleton = new GameObject().AddComponent<MockSingleton>();
                secondSingleton.CallAwake();
            }

            Assert.AreEqual(instanceId, MockSingleton.GetInstance().GetInstanceID());
            Assert.Catch(() =>
            {
                var go = secondSingleton.gameObject;
            });
        }

        [TearDown]
        public void Teardown()
        {
            if (_singleton)
                Object.DestroyImmediate(_singleton.gameObject);
        }
    }
}