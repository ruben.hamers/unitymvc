using System.Linq;
using HamerSoft.Core;
using HamerSoft.Core.Services;
using NUnit.Framework;

namespace HamerSoft.Specs.Core
{
    [TestFixture]
    public class AssemblyTypeCrawlerTests
    {
        [Test]
        public void AssemblyTypeCrawler_Finds_Types_Based_On_Interface()
        {
            var genericAssemblyTypeCrawler = new AssemblyTypeCrawler<IService>();
            var foundTypes = genericAssemblyTypeCrawler.GetTypes();
            Assert.True(foundTypes.Sanitize().ToArray().Length > 0);
        }

        [Test]
        public void AssemblyTypeCrawler_Finds_Types_Based_On_Class()
        {
            var genericAssemblyTypeCrawler = new AssemblyTypeCrawler<Scriptable>();
            var foundTypes = genericAssemblyTypeCrawler.GetTypes();
            Assert.True(foundTypes.Sanitize().ToArray().Length > 0);
        }

        [Test]
        public void GenericAssemblyTypeCrawler_Finds_Types_When_No_Generic_Is_Given()
        {
            var genericAssemblyTypeCrawler = new GenericAssemblyTypeCrawler<IService>(null);
            var foundTypes = genericAssemblyTypeCrawler.GetTypes();
            Assert.True(foundTypes.Sanitize().ToArray().Length > 0);
        }

        [Test]
        public void GenericAssemblyTypeCrawler_Finds_Types_As_Dictionary_When_No_Generic_Is_Given()
        {
            var genericAssemblyTypeCrawler = new GenericAssemblyTypeCrawler<IService>(null);
            var foundTypes = genericAssemblyTypeCrawler.GetTypesAsDictionary();
            Assert.True(foundTypes.Sanitize().ToArray().Length > 0);
        }
    }
}