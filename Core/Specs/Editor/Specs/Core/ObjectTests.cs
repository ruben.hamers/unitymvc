using System;
using HamerSoft.Core;
using NUnit.Framework;
using UnityEngine;
using Object = HamerSoft.Core.Object;

namespace HamerSoft.Specs.Core
{
    [TestFixture]
    public class ObjectTests
    {
        private class MockObject : Object
        {
            public void CallAwake()
            {
                Awake();
            }

            public void DelayCall(float delay, Action cb)
            {
                InvokeDelayed(delay, cb);
            }
        }

        private MockObject _object;

        [SetUp]
        public void Setup()
        {
            _object = new GameObject("O").AddComponent<MockObject>();
            _object.CallAwake();
        }

        [Test]
        public void Delayed_Function_Will_Be_Invoked_After_Correct_Delay()
        {
            _object.DelayCall(1, () =>
            {
                if (Application.isEditor)
                    Debug.Log("yes yes");
                Assert.Pass("Called");
                _object.ForceDestroyObject();
            });
        }
    }
}