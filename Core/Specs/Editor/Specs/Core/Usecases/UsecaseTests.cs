using System.Reflection;
using HamerSoft.Core.UseCases;
using NUnit.Framework;

namespace HamerSoft.Core.Specs.UseCases
{
    public class UsecaseTests
    {
        internal class MockUseCase : UseCase
        {
            internal bool Disposed;

            public object GetCallback()
            {
                return typeof(UseCase).GetField("_callback", BindingFlags.Instance | BindingFlags.NonPublic)
                    ?.GetValue(this);
            }

            public void InvokeCallBack()
            {
                InvokeCallback();
            }

            protected override void DisposeUseCase()
            {
                Disposed = true;
            }
        }

        private class MockUseCase1 : UseCase<bool>
        {
            internal bool Disposed;

            public object GetCallback()
            {
                return typeof(UseCase<bool>)
                    .GetField("_callback", BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(this);
            }

            public void InvokeCallBack()
            {
                InvokeCallback(true);
            }

            protected override void DisposeUseCase()
            {
                Disposed = true;
            }
        }

        private class NullableMockUseCase1 : UseCase<string>
        {
            internal bool Disposed;

            public object GetCallback()
            {
                return typeof(UseCase<string>)
                    .GetField("_callback", BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(this);
            }

            public void InvokeCallBack()
            {
                InvokeCallback(null);
            }

            protected override void DisposeUseCase()
            {
                Disposed = true;
            }
        }

        private class MockUseCase2 : UseCase<bool, bool>
        {
            internal bool Disposed;

            public object GetCallback()
            {
                return typeof(UseCase<bool, bool>)
                    .GetField("_callback", BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(this);
            }

            public void InvokeCallBack()
            {
                InvokeCallback(true, true);
            }

            protected override void DisposeUseCase()
            {
                Disposed = true;
            }
        }

        private class MockUseCase3 : UseCase<bool, bool, bool>
        {
            internal bool Disposed;

            public object GetCallback()
            {
                return typeof(UseCase<bool, bool, bool>)
                    .GetField("_callback", BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(this);
            }

            public void InvokeCallBack()
            {
                InvokeCallback(true, true, true);
            }

            protected override void DisposeUseCase()
            {
                Disposed = true;
            }
        }

        private class MockUseCase4 : UseCase<bool, bool, bool, bool>
        {
            internal bool Disposed;

            public object GetCallback()
            {
                return typeof(UseCase<bool, bool, bool, bool>)
                    .GetField("_callback", BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(this);
            }

            public void InvokeCallBack()
            {
                InvokeCallback(true, true, true, true);
            }

            protected override void DisposeUseCase()
            {
                Disposed = true;
            }
        }

        [Test]
        public void When_UseCase_Is_Executed_The_Callback_Is_Set()
        {
            var useCase = new MockUseCase();
            useCase.Execute(() => { });
            Assert.NotNull(useCase.GetCallback());
        }

        [Test]
        public void When_UseCase_Callback_Is_Invoked_The_Callback_Is_Null()
        {
            var useCase = new MockUseCase();
            useCase.Execute(() => { });
            useCase.InvokeCallBack();
            Assert.Null(useCase.GetCallback());
        }

        [Test]
        public void When_UseCase_Callback_Is_Invoked_UseCase_Is_Disposed()
        {
            var useCase = new MockUseCase();
            useCase.Execute(() => { });
            useCase.InvokeCallBack();
            Assert.True(useCase.Disposed);
        }

        [Test]
        public void When_UseCase1_Is_Executed_The_Callback_Is_Set()
        {
            var useCase = new MockUseCase1();
            useCase.Execute((b) => { });
            Assert.NotNull(useCase.GetCallback());
        }

        [Test]
        public void When_UseCase1_Callback_Is_Invoked_The_Callback_Is_Null()
        {
            var useCase = new MockUseCase1();
            useCase.Execute((b) => { });
            useCase.InvokeCallBack();
            Assert.Null(useCase.GetCallback());
        }

        [Test]
        public void When_NullableUseCase1_Callback_Is_Invoked_The_Callback_Returns_Null()
        {
            var useCase = new NullableMockUseCase1();
            useCase.Execute(Assert.Null);
            useCase.InvokeCallBack();
        }

        [Test]
        public void When_UseCase1_Callback_Is_Invoked_UseCase_Is_Disposed()
        {
            var useCase = new MockUseCase1();
            useCase.Execute((b) => { });
            useCase.InvokeCallBack();
            Assert.True(useCase.Disposed);
        }

        [Test]
        public void When_UseCase2_Is_Executed_The_Callback_Is_Set()
        {
            var useCase = new MockUseCase2();
            useCase.Execute((a, b) => { });
            Assert.NotNull(useCase.GetCallback());
        }

        [Test]
        public void When_UseCase2_Callback_Is_Invoked_The_Callback_Is_Null()
        {
            var useCase = new MockUseCase2();
            useCase.Execute((a, b) => { });
            useCase.InvokeCallBack();
            Assert.Null(useCase.GetCallback());
        }

        [Test]
        public void When_UseCase2_Callback_Is_Invoked_UseCase_Is_Disposed()
        {
            var useCase = new MockUseCase2();
            useCase.Execute((a, b) => { });
            useCase.InvokeCallBack();
            Assert.True(useCase.Disposed);
        }

        [Test]
        public void When_UseCase3_Is_Executed_The_Callback_Is_Set()
        {
            var useCase = new MockUseCase3();
            useCase.Execute((a, b, c) => { });
            Assert.NotNull(useCase.GetCallback());
        }

        [Test]
        public void When_UseCase3_Callback_Is_Invoked_The_Callback_Is_Null()
        {
            var useCase = new MockUseCase3();
            useCase.Execute((a, b, c) => { });
            useCase.InvokeCallBack();
            Assert.Null(useCase.GetCallback());
        }

        [Test]
        public void When_UseCase3_Callback_Is_Invoked_UseCase_Is_Disposed()
        {
            var useCase = new MockUseCase3();
            useCase.Execute((a, b, c) => { });
            useCase.InvokeCallBack();
            Assert.True(useCase.Disposed);
        }

        [Test]
        public void When_UseCase4_Is_Executed_The_Callback_Is_Set()
        {
            var useCase = new MockUseCase4();
            useCase.Execute((a, b, c, d) => { });
            Assert.NotNull(useCase.GetCallback());
        }

        [Test]
        public void When_UseCase4_Callback_Is_Invoked_The_Callback_Is_Null()
        {
            var useCase = new MockUseCase4();
            useCase.Execute((a, b, c, d) => { });
            useCase.InvokeCallBack();
            Assert.Null(useCase.GetCallback());
        }

        [Test]
        public void When_UseCase4_Callback_Is_Invoked_UseCase_Is_Disposed()
        {
            var useCase = new MockUseCase4();
            useCase.Execute((a, b, c, d) => { });
            useCase.InvokeCallBack();
            Assert.True(useCase.Disposed);
        }
    }
}