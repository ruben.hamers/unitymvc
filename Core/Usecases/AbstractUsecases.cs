using System;

namespace HamerSoft.Core.UseCases
{
    public abstract class UseCase : IDisposable
    {
        private Action _callback;

        public virtual void Execute(Action callback)
        {
            _callback = callback;
        }

        protected void InvokeCallback()
        {
            _callback?.Invoke();
            Dispose();
        }

        protected abstract void DisposeUseCase();

        public void Dispose()
        {
            _callback = null;
            DisposeUseCase();
        }
    }

    public abstract class UseCase<T1> : IDisposable
    {
        private Action<T1> _callback;

        public virtual void Execute(Action<T1> callback)
        {
            _callback = callback;
        }

        protected void InvokeCallback(T1 first)
        {
            _callback?.Invoke(first);
            Dispose();
        }

        protected abstract void DisposeUseCase();

        public void Dispose()
        {
            _callback = null;
            DisposeUseCase();
        }
    }

    public abstract class UseCase<T1, T2> : IDisposable
    {
        private Action<T1, T2> _callback;

        public virtual void Execute(Action<T1, T2> callback)
        {
            _callback = callback;
        }

        protected void InvokeCallback(T1 first, T2 second)
        {
            _callback?.Invoke(first, second);
            Dispose();
        }

        protected abstract void DisposeUseCase();

        public void Dispose()
        {
            _callback = null;
            DisposeUseCase();
        }
    }

    public abstract class UseCase<T1, T2, T3> : IDisposable
    {
        private Action<T1, T2, T3> _callback;

        public virtual void Execute(Action<T1, T2, T3> callback)
        {
            _callback = callback;
        }

        protected void InvokeCallback(T1 first, T2 second, T3 third)
        {
            _callback?.Invoke(first, second, third);
            Dispose();
        }

        protected abstract void DisposeUseCase();

        public void Dispose()
        {
            _callback = null;
            DisposeUseCase();
        }
    }

    public abstract class UseCase<T1, T2, T3, T4> : IDisposable
    {
        private Action<T1, T2, T3, T4> _callback;

        public virtual void Execute(Action<T1, T2, T3, T4> callback)
        {
            _callback = callback;
        }

        protected void InvokeCallback(T1 first, T2 second, T3 third, T4 fourth)
        {
            _callback?.Invoke(first, second, third, fourth);
            Dispose();
        }

        protected abstract void DisposeUseCase();

        public void Dispose()
        {
            _callback = null;
            DisposeUseCase();
        }
    }
}