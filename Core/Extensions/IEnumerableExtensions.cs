using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace HamerSoft.Core
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> Sanitize<T>(this IEnumerable<T> enumerable)
        {
            return enumerable?.Where(element => element != null);
        }

        public static Dictionary<Type,T> ToTypedDictionary<T>(this IEnumerable<T> enumerable)
        {
            return enumerable?.Sanitize().ToDictionary(v => v.GetType(), v => v);
        }
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> iEnumerable)
        {
            List<T> list = iEnumerable.ToList(); 
            if (list.Count == 0)
                return list;
            Random rng = new Random();
            int n = list.Count;
            T[] values = list.ToArray();
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = values[k];
                values[k] = values[n];
                values[n] = value;
            }

            return values.Where(v => true);
        }
    }
}