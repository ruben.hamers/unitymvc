using System;
using System.Collections.Generic;
using System.Linq;

namespace HamerSoft.Core
{
    public static class DictionaryExtensions
    {
        public static void SafeForeach<Tkey, Tvalue>(this Dictionary<Tkey, Tvalue> dictionary,
            Action<Tkey, Tvalue> action)
        {
            for (int index = 0; index < dictionary.Count; index++)
            {
                var item = dictionary.ElementAt(index);
                action.Invoke(item.Key,item.Value);
            }
        }
    }
}