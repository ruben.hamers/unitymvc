using System;
using UnityEngine;

namespace HamerSoft.Core
{
    public static class MonoBehaviourExtensions
    {
        public static void ForceDestroyComponent(this MonoBehaviour target)
        {
            try
            {
                if (Application.isPlaying)
                    UnityEngine.Object.Destroy(target);
                else
                    UnityEngine.Object.DestroyImmediate(target);
            }
            catch (Exception e)
            {
            }
        }

        public static void ForceDestroyObject(this MonoBehaviour target)
        {
            try
            {
                if (Application.isPlaying)
                    UnityEngine.Object.Destroy(target.gameObject);
                else
                    UnityEngine.Object.DestroyImmediate(target.gameObject);
            }
            catch (Exception e)
            {
                Debug.Log($"Tried force destroy but failed with {e} and {e.Message}");
            }
        }
    }
}