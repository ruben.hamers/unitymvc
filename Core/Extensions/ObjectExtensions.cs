using System;
using UnityEngine;

namespace HamerSoft.Core
{
    public static class ObjectExtensions
    {
        public static T GetArgument<T>(this object[] args, int index)
        {
            try
            {
                return (T) args[index];
            }
            catch (Exception)
            {
                Debug.LogWarning(
                    $"Could not cast args with length {args?.Length} and index {index} to desired type {typeof(T)}. Returning default!");
            }

            return default;
        }
    }
}