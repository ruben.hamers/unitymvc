using UnityEngine;

namespace HamerSoft.Core
{
    public static class Vector3Extensions
    {
        public static bool AlmostEquals(this Vector3 v1, Vector3 v2, float precision)
        {
            return v1.x.AlmostEquals(v2.x, precision) &&
                   v1.y.AlmostEquals(v2.y, precision) &&
                   v1.z.AlmostEquals(v2.z, precision);
        }
    }
}