using UnityEngine;

namespace HamerSoft.Core
{
    public static class FloatExtensions
    {
        public static bool AlmostEquals(this float f1, float f2, float precision)
        {
            return Mathf.Abs(f1- f2) <= precision;
        }
    }
}