using UnityEngine.Events;
using UnityEngine.UI;

namespace HamerSoft.Core
{
    public static class ButtonExtensions
    {
        public static void AddListener(this Button button, UnityAction callback)
        {
            if (button)
                button.onClick.AddListener(callback);
        }
    }
}