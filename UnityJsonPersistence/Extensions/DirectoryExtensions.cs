using System.IO;

namespace HamerSoft.UnityJsonPersistence.Extensions
{
    public static class DirectoryExtensions
    {
        public static void DeleteAll(this DirectoryInfo dir)
        {
            foreach(FileInfo fi in dir.GetFiles())
                fi.Delete();

            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                DeleteAll(di);
                di.Delete();
            }
        }
    }
}