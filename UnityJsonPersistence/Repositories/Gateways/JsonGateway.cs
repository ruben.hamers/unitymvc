using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HamerSoft.Core;
using Newtonsoft.Json;
using UnityEngine;

namespace HamerSoft.UnityJsonPersistence
{
    public abstract class JsonGateway<T> : IGateway<T> where T : IIdentifiable
    {
        /// <summary>
        /// subdirectory local to persistentDataPath (windows) <user>/AppData/locallow/companyname/project/<subdirectory>/..
        /// </summary>
        protected abstract string SubDirectory { get; }

        /// <summary>
        /// unity3d's persistent data path, we cache it since the json gateway might include multithreading, and the persistentdatapath is not accessible from another thread.
        /// persistent data path relates to (windows) <user>/AppData/locallow/companyname/project/..
        /// </summary>
        private readonly string _persistentDataPath;

        public JsonGateway()
        {
            _persistentDataPath = UnityEngine.Application.persistentDataPath;
            if (!Directory.Exists(GetPath()))
                Directory.CreateDirectory(GetPath());
        }

        private string GetPath()
        {
            return Path.Combine(_persistentDataPath, SubDirectory);
        }

        private string GetFullFilePath(string id)
        {
            return Path.Combine(GetPath(), $"{id}.json");
        }

        public void Persist(T value)
        {
            string jsonData = JsonConvert.SerializeObject(value);
            Write(jsonData, GetFullFilePath(value.Id));
        }

        public void Delete(T value)
        {
            if (Exists(value.Id))
                File.Delete(GetFullFilePath(value.Id));
        }

        public bool Exists(string id)
        {
            return File.Exists(GetFullFilePath(id));
        }

        public IEnumerable<T> LoadLazy(List<string> idsInMemory)
        {
            string[] files = Directory.GetFiles(GetPath());
            if (files.Length == 0)
                yield break;

            
            foreach (string file in files)
            {
                if (!idsInMemory.Any(id => file.Contains(id)))
                    yield return Read(file);
            }
        }

        /// <summary>
        /// write async to save performance (when Json files get large, this becomes an issue and the Unityengine will freeze up for a couple of frames)
        /// </summary>
        /// <param name="data">data (json)</param>
        /// <param name="path">file path</param>
        /// <returns>async write task</returns>
        private async Task Write(string data, string path)
        {
            using (StreamWriter outputFile = new StreamWriter(path))
            {
                await outputFile.WriteAsync(data);
            }
        }

        /// <summary>
        /// load async to save performance (when Json files get large, this becomes an issue and the Unityengine will freeze up for a couple of frames)
        /// </summary>
        /// <param name="id">file Id(json)</param>
        /// <param name="callback">callback when done reading the file and deserialized</param>
        public void Load(string id, Action<T> callback)
        {
            T value = default;
            try
            {
                value = Read(GetFullFilePath(id));
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Loading file with Id:{id} failed! Exception: {e} | {e.Message}");
            }
            finally
            {
                callback.Invoke(value);
            }
        }

        /// <summary>
        /// load async to save performance (when Json files get large, this becomes an issue and the Unityengine will freeze up for a couple of frames)
        /// </summary>
        /// <param name="path">file Id(json)</param>
        /// <param name="callback">callback when done reading the file and deserialized</param>
        private void LoadPath(string path, Action<T> callback)
        {
            T value = default;
            try
            {
                value = Read(path);
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Loading file with Id:{path} failed! Exception: {e} | {e.Message}");
            }
            finally
            {
                callback.Invoke(value);
            }
        }

        /// <summary>
        /// read async to save performance
        /// </summary>
        /// <param name="path">file path</param>
        /// <returns></returns>
        private T Read(string path)
        {
            try
            {
                using (var sr = new StreamReader(path))
                {
                    string content = sr.ReadToEndAsync().Result;
                    return JsonConvert.DeserializeObject<T>(content);
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}