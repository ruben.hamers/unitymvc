using System.Collections;
using System.IO;
using HamerSoft.UnityJsonPersistence.Extensions;
using HamerSoft.Core;
using HamerSoft.UnityJsonPersistence.Specs.Mocks;
using Newtonsoft.Json;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace HamerSoft.UnityJsonPersistence.Editor.Specs
{
    public class JsonRepositoryTests
    {
        private class MockRepositoryListener : IRepositoryListener<MockPersistable>
        {
            public MockPersistable Added { get; set; }
            public bool Removed { get; set; }
            public MockPersistable Updated { get; set; }

            public void AddedObject(MockPersistable added)
            {
                Added = added;
            }

            public void RemovedObject(MockPersistable removed)
            {
                Removed = true;
            }

            public void UpdatedObject(MockPersistable updated)
            {
                Updated = updated;
            }
        }

        private class MockGatewayListener : IGatewayListener<MockPersistable>
        {
            public MockPersistable Persisted { get; set; }
            public bool Deleted { get; set; }

            public void PersistedObject(MockPersistable added)
            {
                Persisted = added;
            }

            public void DeletedObject(MockPersistable removed)
            {
                Deleted = true;
            }
        }
        
        private MockRepository _mr;
        private MockPersistable _mp;
        private MockRepositoryListener _repositoryListener;
        private MockGatewayListener _gatewayListener;

        [SetUp]
        public void Setup()
        {
            _mr = new MockRepository();
            _mp = new MockPersistable();
            _repositoryListener = new MockRepositoryListener();
            _gatewayListener = new MockGatewayListener();
            _mr.AddListener(_repositoryListener);
            _mr.AddListener(_gatewayListener);
        }

        [Test]
        public void Upon_Construction_Directory_Is_Created()
        {
            Assert.True(Directory.Exists(MockRepository.Repo));
        }

        [Test]
        public void Upon_Construction_Directory_Is_Empty()
        {
            Assert.True(Directory.GetFiles(MockRepository.Repo).Length == 0);
        }

        [Test]
        public void When_Object_Is_Staged_It_Is_Not_Yet_Persisted()
        {
            _mr.AddObject(_mp);
            Assert.True(Directory.GetFiles(MockRepository.Repo).Length == 0);
        }
        [Test]
        public void Has_Object_First_Checks_InMemory_Objects_Before_Checking_Persisted_Files()
        {
            _mr.AddObject(_mp);
            Assert.True(Directory.GetFiles(MockRepository.Repo).Length == 0);
            Assert.True(_mr.HasObject(_mp));
        }
        
        [Test]
        public void RemoveObject_Is_Able_To_Remove_Files_Directly_From_Disk_Without_Having_Them_In_Memory()
        {
            File.WriteAllText(Path.Combine(Application.persistentDataPath, MockJsonGateway.Directory,
                $"{_mp.Id}.json"), JsonConvert.SerializeObject(_mp));
            Assert.True(Directory.GetFiles(MockRepository.Repo).Length == 1);
            _mr.RemoveObject(ref _mp);
            Assert.True(Directory.GetFiles(MockRepository.Repo).Length == 0);
        }
        [Test]
        public void GetObjectBy_Finds_Objects_In_Memory()
        {
            _mr.AddObject(_mp);
            Assert.NotNull(_mr.Get(persistable => persistable.Id == _mp.Id));
        }
        [Test]
        public void GetObjectBy_Finds_Objects_On_Disk()
        {
            File.WriteAllText(Path.Combine(Application.persistentDataPath, MockJsonGateway.Directory,
                $"{_mp.Id}.json"), JsonConvert.SerializeObject(_mp));
            Assert.NotNull(_mr.Gets(persistable => persistable.Id == _mp.Id));
        }
        [Test]
        public void GetObjectsBy_Finds_Objects_In_Memory()
        {
            _mr.AddObject(_mp);
            Assert.True(_mr.Gets(persistable => persistable.Id == _mp.Id).Count != 0);
        }
        [Test]
        public void GetObjectsBy_Finds_Objects_On_Disk()
        {
            File.WriteAllText(Path.Combine(Application.persistentDataPath, MockJsonGateway.Directory,
                $"{_mp.Id}.json"), JsonConvert.SerializeObject(_mp));
            Assert.True(_mr.Gets(persistable => persistable.Id == _mp.Id).Count > 0);
        }

        [Test]
        public void When_Object_Is_Added_The_RepositoryListener_Gets_Notified()
        {
            _mr.AddObject(_mp);
            Assert.NotNull(_repositoryListener.Added);
        }
        [Test]
        public void When_Object_Is_Updated_The_RepositoryListener_Gets_Notified()
        {
            _mr.AddObject(_mp);
            _mr.UpdateObject(_mp);
            Assert.NotNull(_repositoryListener.Updated);
        }
        [Test]
        public void When_Object_Is_Removed_The_RepositoryListener_Gets_Notified()
        {
            _mr.AddObject(_mp);
            _mr.RemoveObject(ref _mp);
            Assert.True(_repositoryListener.Removed);
        }
        
        [Test]
        public void After_RepositoryListener_Is_Removed_When_Object_Is_Added_The_RepositoryListener_Is_Not_Notified()
        {
            _mr.RemoveListener(_repositoryListener);
            _mr.AddObject(_mp);
            Assert.Null(_repositoryListener.Added);
        }
        [Test]
        public void  After_RepositoryListener_Is_Removed_When_Object_Is_Updated_The_RepositoryListener_Is_Not_Notified()
        {
            _mr.RemoveListener(_repositoryListener);
            _mr.AddObject(_mp);
            _mr.UpdateObject(_mp);
            Assert.Null(_repositoryListener.Updated);
        }
        [Test]
        public void  After_RepositoryListener_Is_Removed_When_Object_Is_Removed_The_RepositoryListener_Is_Not_Notified()
        {
            _mr.RemoveListener(_repositoryListener);
            _mr.AddObject(_mp);
            _mr.RemoveObject(ref _mp);
            Assert.False(_repositoryListener.Removed);
        }
        
        [Test]
        public void When_Object_Is_Persisted_The_GatewayListener_Gets_Notified()
        {
            _mr.AddObject(_mp);
            _mr.Flush();
            Assert.NotNull(_gatewayListener.Persisted);
        }
                
        [Test]
        public void When_Object_Is_Deleted_The_GatewayListener_Gets_Notified()
        {
            _mr.AddObject(_mp);
            _mr.RemoveObject(ref _mp);
            _mr.Flush();
            Assert.True(_gatewayListener.Deleted);
        }
        
        [Test]
        public void After_GateWayListener_Is_Removed_When_Object_Is_Persisted_The_GatewayListener_Is_Not_Notified()
        {
            _mr.RemoveListener(_gatewayListener);
            _mr.AddObject(_mp);
            _mr.Flush();
            Assert.Null(_gatewayListener.Persisted);
        }
                
        [Test]
        public void After_GateWayListener_Is_Removed_When_Object_Is_Deleted_The_GatewayListener_Is_Not_Notified()
        {
            _mr.RemoveListener(_gatewayListener);
            _mr.AddObject(_mp);
            _mr.RemoveObject(ref _mp);
            _mr.Flush();
            Assert.False(_gatewayListener.Deleted);
        }

        [TearDown]
        public void TearDown()
        {
            if (Directory.Exists(MockRepository.Repo))
                new DirectoryInfo(MockRepository.Repo).DeleteAll();
            _mr.Dispose();
        }
    }
}