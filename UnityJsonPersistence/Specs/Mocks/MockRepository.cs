using System;
using System.Collections.Generic;
using HamerSoft.Core;
using Newtonsoft.Json;
using UnityEngine;

namespace HamerSoft.UnityJsonPersistence.Specs.Mocks
{
    public class MockPersistable : IIdentifiable
    {
        [JsonProperty] public string Id { get; }
        [JsonProperty] public string Message { get; private set; }

        public MockPersistable()
        {
            Id = "HelloWorld";
            Message = "HelloWorld";
        }

        public MockPersistable UpdateMessage(string message)
        {
            Message = message;
            return this;
        }

        public void Dispose()
        {
        }
    }

    public class MockJsonGateway : JsonGateway<MockPersistable>
    {
        protected override string SubDirectory => Directory;
        public static string Directory => "Specs/JsonRepositoryTests";
    }

    public class MockRepository : Repository<MockPersistable>
    {
        protected override IGateway<MockPersistable> CreateGateway()
        {
            return new MockJsonGateway();
        }

        public MockPersistable Get(Func<MockPersistable, bool> filter)
        {
            return GetObjectBy(filter);
        }

        public List<MockPersistable> Gets(Func<MockPersistable, bool> filter)
        {
            return GetObjectsBy(filter);
        }

        public static string Repo => $"{Application.persistentDataPath}/{MockJsonGateway.Directory}";
    }
}