using System.Collections;
using System.IO;
using HamerSoft.UnityJsonPersistence.Extensions;
using HamerSoft.UnityJsonPersistence.Specs.Mocks;
using Newtonsoft.Json;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace HamerSoft.UnityJsonPersistence.Specs
{
    public class RunTimeJsonRepositoryTests
    {
        private MockRepository mr;
        private MockPersistable mp;

        [SetUp]
        public void Setup()
        {
            mr = new MockRepository();
            mp = new MockPersistable();
        }

        [UnityTest]
        public IEnumerator When_Object_Is_Saved_It_Will_Persisted()
        {
            AddAndSaveMockPersistable();
            yield return new WaitForSeconds(1);
            Assert.True(Directory.GetFiles(MockRepository.Repo).Length == 1);
        }

        [UnityTest]
        public IEnumerator When_Object_Is_Updated_It_Is_Not_Yet_Persisted()
        {
            AddAndSaveMockPersistable();
            yield return new WaitForSeconds(1);
            string oldMessage = mp.Message;
            mr.UpdateObject(mp.UpdateMessage("LorumIpsum"));
            MockPersistable mockPersistable = ReadFromDisk();
            Assert.AreEqual(oldMessage, mockPersistable.Message);
        }

        [UnityTest]
        public IEnumerator When_Object_Is_Updated_And_Saved_It_Will_Persisted()
        {
            AddAndSaveMockPersistable();
            yield return new WaitForSeconds(1);
            mr.UpdateObject(mp.UpdateMessage("LorumIpsum"));
            mr.Flush();
            yield return new WaitForSeconds(1);
            Assert.True(Directory.GetFiles(MockRepository.Repo).Length == 1);
            MockPersistable mockPersistable = ReadFromDisk();
            Assert.AreEqual("LorumIpsum", mockPersistable.Message);
        }

        [UnityTest]
        public IEnumerator When_Object_Is_Deleted_It_Is_Not_Yet_Removed_From_Disk()
        {
            AddAndSaveMockPersistable();
            yield return new WaitForSeconds(1);
            mr.RemoveObject(ref mp);
            Assert.True(Directory.GetFiles(MockRepository.Repo).Length == 1);
        }

        [UnityTest]
        public IEnumerator When_Object_Is_Deleted_And_Saved_It_Will_Removed_From_Disk()
        {
            AddAndSaveMockPersistable();
            yield return new WaitForSeconds(1);
            mr.RemoveObject(ref mp);
            mr.Flush();
            yield return new WaitForSeconds(1);
            Assert.True(Directory.GetFiles(MockRepository.Repo).Length == 0);
        }

        [UnityTest]
        public IEnumerator Has_Object_Checks_Persisted_Files_When_Object_Is_Not_In_Memory()
        {
            WriteOutToDisk();
            yield return new WaitForSeconds(1);
            Assert.True(Directory.GetFiles(MockRepository.Repo).Length == 1);
            Assert.True(mr.HasObject(mp));
        }

        [UnityTest]
        public IEnumerator Get_Object_Returns_Persisted_Files_When_Object_Is_Not_In_Memory()
        {
            WriteOutToDisk();
            yield return new WaitForSeconds(1);
            MockPersistable mockPersistable = mr.GetObject(mp.Id);
            Assert.True(mr.HasObject(mockPersistable));
        }

        private static void ClearTestPersistenceRepo()
        {
            if (Directory.Exists(MockRepository.Repo))
                new DirectoryInfo(MockRepository.Repo).DeleteAll();
        }

        private MockPersistable ReadFromDisk()
        {
            return JsonConvert.DeserializeObject<MockPersistable>(
                File.ReadAllText(Path.Combine(Application.persistentDataPath, MockJsonGateway.Directory,
                    $"{mp.Id}.json")));
        }

        private void WriteOutToDisk()
        {
            File.WriteAllText(Path.Combine(Application.persistentDataPath, MockJsonGateway.Directory,
                $"{mp.Id}.json"), JsonConvert.SerializeObject(mp));
        }

        private void AddAndSaveMockPersistable()
        {
            mr.AddObject(mp);
            mr.Flush();
        }

        [TearDown]
        public void TearDown()
        {
            ClearTestPersistenceRepo();
        }
    }
}