using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using HamerSoft.Core;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using Object = HamerSoft.Core.Object;

namespace HamerSoft.Audio
{
    public enum AudioGroup
    {
        Ambient = 0,
        Effects = 1,
        Master = 2,
        Music = 3,
        Voice = 4,
        World = 5
    }

    [CreateAssetMenu(fileName = "AudioPlugin", menuName = "HamerSoft/Audio/AudioPlugin Create", order = 0)]
    public class AudioPlugin : Plugin
    {
        private Dictionary<int, AudioPlayer> _audioPlayers;
        [SerializeField] private AudioMixer audioMixer;
        private Dictionary<AudioGroup, AudioMixerGroup> _groups;

        public override void SetMain(Main main)
        {
            MapAudioGroups();
        }

        private void MapAudioGroups()
        {
            _groups = new Dictionary<AudioGroup, AudioMixerGroup>();
            if (!audioMixer)
                return;
            foreach (AudioMixerGroup matchingGroup in audioMixer.FindMatchingGroups("Master"))
                switch (matchingGroup.name.ToLower())
                {
                    case "ambient":
                        _groups.Add(AudioGroup.Ambient, matchingGroup);
                        break;
                    case "effects":
                        _groups.Add(AudioGroup.Effects, matchingGroup);
                        break;
                    case "music":
                        _groups.Add(AudioGroup.Music, matchingGroup);
                        break;
                    case "voice":
                        _groups.Add(AudioGroup.Voice, matchingGroup);
                        break;
                    case "world":
                        _groups.Add(AudioGroup.World, matchingGroup);
                        break;
                }
        }

        public override void Init()
        {
            if (Application.isPlaying)
                _audioPlayers = SceneManager.GetActiveScene().GetRootGameObjects()
                    .SelectMany(rgo => rgo.GetComponentsInChildren<AudioPlayer>())
                    .ToDictionary(kvp => kvp.GetInstanceID(), kvp => kvp);
            else
                _audioPlayers = new Dictionary<int, AudioPlayer>();
        }

        public override void DeInit()
        {
            _audioPlayers.SafeForeach((instanceId, player) =>
            {
                if (player.CrossScene)
                    return;
                player.ForceDestroyObject();
                _audioPlayers.Remove(instanceId);
            });
        }

        public override void ApplicationQuit()
        {
            _audioPlayers.SafeForeach((instanceId, player) =>
            {
                player.ForceDestroyObject();
                _audioPlayers.Remove(instanceId);
            });
        }

        public string[] GetGroups()
        {
            return audioMixer.FindMatchingGroups("Master").Select(g => g.name).ToArray();
        }

        public AudioMixerGroup GetGroup(AudioGroup group)
        {
            return audioMixer.FindMatchingGroups(group.ToString()).FirstOrDefault();
        }

        private void AudioPlayerDestroyed(Object player)
        {
            var audioPlayer = player as AudioPlayer;
            if (!audioPlayer || !_audioPlayers.ContainsKey(audioPlayer.GetInstanceID()))
                return;
            _audioPlayers.Remove(audioPlayer.Id);
        }

        public AudioPlayer PlayAudio(AudioClip clip, float volume, AudioGroup group, Action callback)
        {
            if (!clip)
            {
                callback?.Invoke();
                return null;
            }
            var audioPlayer = CreatePlayer(clip, null);

            void CallBack()
            {
                callback?.Invoke();
                audioPlayer.ForceDestroyObject();
            }
            
            audioPlayer.PlayAudio(clip, volume, _groups[group], CallBack);
            AddPlayer(clip, audioPlayer);
            return audioPlayer;
        }

        private static AudioPlayer CreatePlayer(AudioClip clip, GameObject target)
        {
            return !target
                ? new GameObject($"AudioPlayer_{clip.name}").AddComponent<AudioPlayer>()
                : target.AddComponent<AudioPlayer>();
        }

        private void AddPlayer(AudioClip clip, AudioPlayer audioPlayer)
        {
            _audioPlayers.Add(audioPlayer.GetInstanceID(), audioPlayer);
            audioPlayer.OnDestroyed += AudioPlayerDestroyed;
        }

        public AudioPlayer PlayAudio(AudioClip clip, float volume, string group, Action callback)
        {
            if (Enum.TryParse<AudioGroup>(group, true, out var g))
                return PlayAudio(clip, volume, g, callback);

            callback?.Invoke();
            return null;
        }

        public AudioPlayer PlayAudio(AudioClip clip, float volume, GameObject target, Action callback)
        {
            if (!target || !clip)
            {
                callback?.Invoke();
                return null;
            }

            var audioPlayer = CreatePlayer(clip, target);
            audioPlayer.PlayAudio(clip, volume, _groups[AudioGroup.World], callback);
            AddPlayer(clip, audioPlayer);
            return audioPlayer;
        }

        public AudioPlayer PlayAudioLooping(AudioClip clip, float volume, GameObject target, bool singleton = true)
        {
            if (!target || !clip)
                return null;
            AudioPlayer player = null;
            if (singleton && (player = _audioPlayers.Values.ToList().FirstOrDefault(ap => ap.Id == clip.GetHashCode())))
                return player;

            var audioPlayer = CreatePlayer(clip, target);
            audioPlayer.PlayAudioLooping(clip, volume, _groups[AudioGroup.World], false);
            AddPlayer(clip, audioPlayer);
            return audioPlayer;
        }

        public AudioPlayer PlayAudioLooping(AudioClip clip, float volume, AudioGroup group, bool crossScene,
            bool singleton = true)
        {
            if (!clip)
                return null;
            AudioPlayer player = null;
            if (singleton && (player = _audioPlayers.Values.ToList().FirstOrDefault(ap => ap.Id == clip.GetHashCode())))
                return player;

            var audioPlayer = CreatePlayer(clip, null);
            audioPlayer.PlayAudioLooping(clip, volume, _groups[group], false);
            AddPlayer(clip, audioPlayer);
            return audioPlayer;
        }

        public AudioPlayer PlayAudioLooping(AudioClip clip, float volume, string group, bool crossScene,
            bool singleton = true)
        {
            return Enum.TryParse<AudioGroup>(group, true, out var g)
                ? PlayAudioLooping(clip, volume, g, crossScene, singleton)
                : null;
        }

        public void SetVolume(AudioGroup group, float volume)
        {
            audioMixer.SetFloat($"{group.ToString()}Volume", VolumeToDB(volume));
        }

        public float GetVolume(AudioGroup group)
        {
            return audioMixer.GetFloat($"{group.ToString()}Volume", out var volume) ? DBToVolume(volume) : -1;
        }

        private static float VolumeToDB(float volume)
        {
            return -80 - (-80 * volume);
        }

        private static float DBToVolume(float volume)
        {
            return volume / -80;
        }
    }
}