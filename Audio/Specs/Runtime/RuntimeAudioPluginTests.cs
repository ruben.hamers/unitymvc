using System.Collections;
using HamerSoft.Core;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace HamerSoft.Audio.Specs
{
    public class RuntimeAudioPluginTests
    {
        private AudioPlugin _plugin;
        private AudioPlayer _player;
        private AudioClip _audioClip;
        private Main _main;

        [SetUp]
        public void Setup()
        {
            GetMain();
            _plugin = _main.GetPlugin<AudioPlugin>();
            _audioClip = AudioClip.Create("test", 1, 1, 1000, false);
        }

        [UnityTest]
        public IEnumerator Setting_And_Getting_Volume_Of_A_Group_Sets_And_Returns_The_Correct_Value()
        {
            yield return new WaitForEndOfFrame();
            _plugin.SetVolume(AudioGroup.World,0.5f);
            Assert.AreEqual(0.5f,_plugin.GetVolume(AudioGroup.World));
            _plugin.SetVolume(AudioGroup.World,1);
            yield return new WaitForEndOfFrame();
        }

        protected void GetMain()
        {
            _main =Main.Load();
            _main.Init();
        }

        [TearDown]
        public void TearDown()
        {
            if(_player)
                _player.ForceDestroyObject();
        }
    }
}