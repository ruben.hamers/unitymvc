using HamerSoft.Core;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Audio;
using Object = UnityEngine.Object;

namespace HamerSoft.Audio.Specs
{
    public class AudioPluginTests
    {
        private AudioPlugin _plugin;
        private AudioPlayer _player;
        private AudioClip _audioClip;
        private Main _main;

        [SetUp]
        public void Setup()
        {
            GetMain();
            _plugin = _main.GetPlugin<AudioPlugin>();
            _audioClip = AudioClip.Create("test", 1, 1, 1000, false);
        }

        [Test]
        public void GetGroups_Returns_All_Groups_In_The_Mixer()
        {
            var groups = _plugin.GetGroups();
            Assert.AreEqual(6, groups.Length);
        }

        [Test]
        public void GetGroup_Returns_The_Matching_Audio_Mixer_Group()
        {
            AudioMixerGroup amgAmbient = _plugin.GetGroup(AudioGroup.Ambient);
            AudioMixerGroup amgEffects = _plugin.GetGroup(AudioGroup.Effects);
            AudioMixerGroup amgWorld = _plugin.GetGroup(AudioGroup.World);
            AudioMixerGroup amgVoice = _plugin.GetGroup(AudioGroup.Voice);
            AudioMixerGroup amgMusic = _plugin.GetGroup(AudioGroup.Music);
            AudioMixerGroup amgMaster = _plugin.GetGroup(AudioGroup.Master);

            Assert.NotNull(amgAmbient);
            Assert.NotNull(amgEffects);
            Assert.NotNull(amgWorld);
            Assert.NotNull(amgVoice);
            Assert.NotNull(amgMusic);
            Assert.NotNull(amgMaster);
        }

        [Test]
        public void PlayAudio_With_Enum_Param_Creates_A_New_AudioPlayer_And_Plays_It()
        {
            _player = _plugin.PlayAudio(_audioClip, 0.5f, AudioGroup.World, Assert.Pass);
        }

        [Test]
        public void PlayAudio_With_String_Param_Creates_A_New_AudioPlayer_And_Plays_It()
        {
            _player = _plugin.PlayAudio(_audioClip, 0.5f, "world", Assert.Pass);
        }

        [Test]
        public void PlayAudio_With_GameObject_Param_Creates_A_New_AudioPlayer_And_Plays_It()
        {
            var go = new GameObject();
            _player = _plugin.PlayAudio(_audioClip, 0.5f, go, Assert.Pass);
            Object.DestroyImmediate(go);
        }

        [Test]
        public void PlayAudioLooping_With_Enum_Param_Creates_A_New_AudioPlayer_And_Plays_It()
        {
            _player = _plugin.PlayAudioLooping(_audioClip, 0.5f, AudioGroup.World, false);
            Assert.NotNull(_player);
            _player.ForceDestroyObject();
        }

        [Test]
        public void PlayAudioLooping_With_String_Param_Creates_A_New_AudioPlayer_And_Plays_It()
        {
            _player = _plugin.PlayAudioLooping(_audioClip, 0.5f, "world", false);
            Assert.NotNull(_player);
            _player.ForceDestroyObject();
        }

        [Test]
        public void PlayAudioLooping_With_GameObject_Param_Creates_A_New_AudioPlayer_And_Plays_It()
        {
            var go = new GameObject();
            _player = _plugin.PlayAudioLooping(_audioClip, 0.5f, go, false);
            Object.DestroyImmediate(go);
        }

        [Test]
        public void PlayAudioLooping_As_Singleton_Twice_Returns_The_Same_Player()
        {
            _player = _plugin.PlayAudioLooping(_audioClip, 0.5f, AudioGroup.World, true);
            var instanceId = _player.GetInstanceID();
            _player = _plugin.PlayAudioLooping(_audioClip, 0.5f, AudioGroup.World, true);

            Assert.AreEqual(instanceId, _player.GetInstanceID());
            _player.ForceDestroyObject();
        }

        protected void GetMain()
        {
            _main = Main.Load();
            _main.Init();
        }

        [TearDown]
        public void TearDown()
        {
            if (_player)
                _player.ForceDestroyObject();
        }
    }
}