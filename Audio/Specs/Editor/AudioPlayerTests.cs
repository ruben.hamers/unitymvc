using System.Reflection;
using HamerSoft.Core;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Audio;

namespace HamerSoft.Audio.Specs
{
    public class AudioPlayerTests
    {
        private AudioPlayer _audioPlayer;
        private AudioClip _audioClip;
        private AudioSource _audioSource;

        [SetUp]
        public void Setup()
        {
            _audioPlayer = new GameObject("audioplayer").AddComponent<AudioPlayer>();
            _audioClip = AudioClip.Create("test", 1, 1, 1000, false);
            _audioSource = _audioPlayer.GetComponent<AudioSource>();
            InvokeAwake();
        }

        [Test]
        public void When_Play_Is_Invoked_The_AudioSource_Values_Are_Set_Correctly()
        {
            _audioPlayer.PlayAudio(_audioClip, 0.5f, null, () => { });
            Assert.True(_audioSource.isPlaying);
            Assert.False(_audioSource.loop);
            Assert.NotNull(_audioSource.clip);
            Assert.AreEqual(0.5f, _audioSource.volume);
            Assert.Null(_audioSource.outputAudioMixerGroup);
        }

        [Test]
        public void When_PlayLooping_Is_Invoked_The_AudioSource_Values_Are_Set_Correctly()
        {
            _audioPlayer.PlayAudioLooping(_audioClip, 0.5f, null,false);
            Assert.True(_audioSource.isPlaying);
            Assert.NotNull(_audioSource.clip);
            Assert.True(_audioSource.loop);
            Assert.AreEqual(0.5f, _audioSource.volume);
            Assert.Null(_audioSource.outputAudioMixerGroup);
        }
        
        [Test]
        public void When_AudioPlayer_Is_Stopped_It_Will_Invoke_The_Callback()
        {
            _audioPlayer.PlayAudio(_audioClip, 0.5f, null, () => Assert.Pass("callbackCalled"));
            _audioPlayer.Stop();
        }
        
        [Test]
        public void When_AudioPlayer_Is_ForceStopped_It_Will_Not_Invoke_The_Callback()
        {
            bool invoked = false;
            _audioPlayer.PlayAudio(_audioClip, 0.5f, null, () => invoked = true);
            _audioPlayer.ForceStop();
            
            int i = 0;
            while (i < 10000)
            {
                if(invoked)
                    Debug.Log($"invoked = true;");

                i++;
            }
            Assert.False(invoked);
        }

        private void InvokeAwake()
        {
            typeof(AudioPlayer).GetMethod("Awake", BindingFlags.Instance | BindingFlags.NonPublic)
                .Invoke(_audioPlayer, null);
        }

        [TearDown]
        public void Teardown()
        {
            _audioPlayer.ForceDestroyObject();
        }
    }
}