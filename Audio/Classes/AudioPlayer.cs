using System;
using System.Threading.Tasks;
using HamerSoft.Core;
using UnityEngine;
using UnityEngine.Audio;
using Object = HamerSoft.Core.Object;

namespace HamerSoft.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioPlayer : Object
    {
        private AudioSource _audioSource;
        public bool CrossScene { get; protected set; }
        public int Id { get; protected set; }
        private DelayedCall _callbackTask;
        private Action _callback;

        protected override void Awake()
        {
            base.Awake();
            GetAudioSource();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            ForceStop();
        }

        private void GetAudioSource()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        public void PlayAudio(AudioClip clip, float volume, AudioMixerGroup group, Action callback)
        {
            GetAudioSource();
            Id = clip.GetHashCode();
            ConfigAudioSource(clip, volume, group, false);
            _callback = callback;
            InvokeClipFinished(clip, callback);
        }

        private void ConfigAudioSource(AudioClip clip, float volume, AudioMixerGroup group, bool looping)
        {
            _audioSource.volume = volume;
            _audioSource.clip = clip;
            _audioSource.outputAudioMixerGroup = group;
            _audioSource.loop = looping;
            _audioSource.Play();
        }

        private void InvokeClipFinished(AudioClip clip, Action callback)
        {
            if (callback != null)
                _callbackTask = InvokeDelayed(clip.length, callback);
        }

        public void PlayAudioLooping(AudioClip clip, float volume, AudioMixerGroup group, bool crossScene)
        {
            GetAudioSource();
            Id = clip.GetHashCode();
            if (crossScene)
                DontDestroyOnLoad(this);
            ConfigAudioSource(clip, volume, group, true);
        }

        public void ForceStop()
        {
            if (_audioSource)
                _audioSource.Stop();
            _callbackTask?.Stop();
            _callbackTask = null;
            _callback = null;
        }

        public void Stop()
        {
            if (_audioSource)
                _audioSource.Stop();
            _callback?.Invoke();
            _callbackTask = null;
            _callback = null;
        }
    }
}