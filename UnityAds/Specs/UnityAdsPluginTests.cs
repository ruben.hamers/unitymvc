using System.Reflection;
using HamerSoft.Core;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Advertisements;

namespace HamerSoft.UnityAds.Specs
{
    [TestFixture]
    public class UnityAdsPluginTests
    {
        private UnityAdsPlugin _plugin;
        private Main _main;

        [SetUp]
        public void Setup()
        {
            GetMain();
            _plugin = _main.GetPlugin<UnityAdsPlugin>();
        }

        [Test]
        public void When_In_Editor_TestMode_Is_Enabled()
        {
            Assert.True(UnityAdsPlugin.IsTestMode);
        }

        [Test]
        public void Upon_Init_The_AdvertisementService_Is_Initialized()
        {
            Assert.True(Advertisement.isInitialized);
        }

        [Test]
        public void Upon_Init_The_Ads_Dictionary_Is_Created()
        {
            Assert.NotNull(typeof(UnityAdsPlugin)
                .GetField("_showableAds", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(_plugin));
        }

        protected void GetMain()
        {
            _main = Main.Load();
            _main.Init();
        }


        public void TearDown()
        {
        }
    }
}