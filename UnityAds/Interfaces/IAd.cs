using System;
using UnityEngine.Advertisements;

namespace HamerSoft.UnityAds
{
    public interface IAd : IUnityAdsListener, IDisposable
    {
        event Action<IAd> Failed, Skipped, Finished, Disposed; 
        string PlacementId { get; }
    }
}