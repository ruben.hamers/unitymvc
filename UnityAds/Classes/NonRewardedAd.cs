using UnityEngine.Advertisements;

namespace HamerSoft.UnityAds
{
    public class NonRewardedAd : AbstractAd
    {
        public NonRewardedAd(string placementId) : base(placementId)
        {
        }

        public override void OnUnityAdsReady(string placementId)
        {
            if (placementId != PlacementId)
                return;
            Advertisement.Show(PlacementId);
        }
    }
}