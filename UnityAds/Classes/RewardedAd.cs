using UnityEngine.Advertisements;

namespace HamerSoft.UnityAds
{
    public class RewardedAd : AbstractAd
    {
        public RewardedAd(string placementId) : base(placementId)
        {
        }

        public void Activate()
        {
            if (IsReady)
                Advertisement.Show(PlacementId);
        }
    }
}