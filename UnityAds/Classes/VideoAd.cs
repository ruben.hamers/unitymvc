using UnityEngine.Advertisements;

namespace HamerSoft.UnityAds
{
    public class VideoAd : AbstractAd
    {
        public VideoAd(string placementId) : base(placementId)
        {
        }

        public override void OnUnityAdsReady(string placementId)
        {
            if (placementId != PlacementId)
                return;

            Advertisement.Show(PlacementId);
        }
    }
}