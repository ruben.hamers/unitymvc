using System.Collections;
using HamerSoft.Core.Services;
using UnityEngine;
using UnityEngine.Advertisements;

namespace HamerSoft.UnityAds
{
    public class BannerAd : AbstractAd
    {
        private BannerPosition _position;
        private Coroutine _coroutine;
        private bool hidden;

        public BannerAd(string placementId, BannerPosition position) : base(placementId)
        {
            _position = position;
        }

        public override void Show()
        {
            if (string.IsNullOrEmpty(PlacementId))
                Debug.Log($"Could not show ad, the PlacementId is empty! Destroyed the Ad!");
            else
                _coroutine = ServiceProvider.GetService<CoroutineService>().StartCoroutine(ShowBannerWhenReady());
        }

        private IEnumerator ShowBannerWhenReady()
        {
            while (!Advertisement.IsReady(PlacementId))
            {
                yield return new WaitForSeconds(0.5f);
            }

            OnUnityAdsReady(PlacementId);
        }

        public override void OnUnityAdsReady(string placementId)
        {
            if (placementId != PlacementId || hidden)
                return;

            Advertisement.Banner.SetPosition(_position);
            Advertisement.Banner.Show(PlacementId);
        }

        public virtual void Hide()
        {
            hidden = true;
            Dispose();
        }

        public override void Dispose()
        {
            hidden = true;
            if (_coroutine != null)
                ServiceProvider.GetService<CoroutineService>().StopCoroutine(_coroutine);
            Advertisement.Banner.Hide(true);
            Advertisement.RemoveListener(this);
            base.Dispose();
        }
    }
}