using UnityEngine.Advertisements;

namespace HamerSoft.UnityAds
{
    public class PersonalizedAd : AbstractAd
    {
        public PersonalizedAd(string placementId) : base(placementId)
        {
        }

        public override void OnUnityAdsReady(string placementId)
        {
            if (placementId != PlacementId)
                return;

            Advertisement.Show(PlacementId);
        }
    }
}