using System;
using UnityEngine;
using UnityEngine.Advertisements;
using Object = HamerSoft.Core.Object;

namespace HamerSoft.UnityAds
{
    public abstract class AbstractAd : IAd, IDisposable
    {
        public event Action<IAd> Failed, Skipped, Finished, Disposed;
        public string PlacementId { get; protected set; }
        public bool IsReady, IsFailed, IsSkipped, IsFinished, IsStarted;

        public AbstractAd(string placementId)
        {
            PlacementId = placementId;
        }

        public virtual void Show()
        {
            if (string.IsNullOrEmpty(PlacementId))
                Debug.Log($"Could not show ad, the PlacementId is empty! Destroyed the Ad!");
            else
            {
                Advertisement.AddListener(this);
                Advertisement.Show(PlacementId);
            }
        }


        public virtual void OnUnityAdsReady(string placementId)
        {
            if (placementId != PlacementId)
                return;
            IsReady = true;
        }

        public virtual void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            if (placementId != PlacementId)
                return;
            Advertisement.RemoveListener(this);
            switch (showResult)
            {
                case ShowResult.Finished:
                    AdFinished();
                    break;
                case ShowResult.Skipped:
                    AdSkipped();
                    break;
                case ShowResult.Failed:
                    AdFailed();
                    break;
            }
        }

        protected virtual void AdFailed()
        {
            Debug.LogWarning($"The ad with PlacementId {PlacementId} got an error!");
            IsFailed = true;
            Failed?.Invoke(this);
        }

        protected virtual void AdSkipped()
        {
            Debug.Log($"The ad with PlacementId {PlacementId} has been skipped!");
            IsSkipped = true;
            Skipped?.Invoke(this);
        }

        protected virtual void AdFinished()
        {
            Debug.Log($"The ad with PlacementId {PlacementId} has finished!");
            Finished?.Invoke(this);
        }

        public virtual void OnUnityAdsDidError(string message)
        {
            Debug.LogWarning($"Error showing ad {PlacementId}, message; {message}");
        }

        public virtual void OnUnityAdsDidStart(string placementId)
        {
            if (placementId != PlacementId)
                return;
            IsStarted = true;
        }

        public virtual void Dispose()
        {
            Disposed?.Invoke(this);
        }
    }
}