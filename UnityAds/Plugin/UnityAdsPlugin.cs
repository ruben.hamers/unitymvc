using System;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core;
using UnityEngine;
using UnityEngine.Advertisements;

namespace HamerSoft.UnityAds
{
    public enum AdType
    {
        Banner,
        PersonalizedPlacement,
        RewardedAd,
        NonRewardedAd,
        Interstitial
    }

    [CreateAssetMenu(fileName = "AdsPlugin", menuName = "HamerSoft/Ads/AdsPlugin Create", order = 0)]
    public class UnityAdsPlugin : Plugin
    {
        [Serializable]
        private struct Ad
        {
            public string PlacementID;
            public AdType Type;
        }

        [SerializeField] private List<Ad> Ads;
        [SerializeField] private int IosGameId, AndroidGameId;
        private Dictionary<AdType, List<string>> _showableAds;
        private Dictionary<string, IAd> _activeAds;

        public static bool IsTestMode => Application.isEditor || Debug.isDebugBuild;

        public override void Init()
        {
            if (Application.isPlaying)
                InitializeAds();
            _activeAds = new Dictionary<string, IAd>();
            GenerateAds();
        }

        private void InitializeAds()
        {
#if UNITY_ANDROID
            Advertisement.Initialize(AndroidGameId.ToString(), IsTestMode);
#elif UNITY_IOS || UNITY_IPHONE
                 Advertisement.Initialize(IosGameId.ToString(), IsTestMode);
#endif
        }

        private void GenerateAds()
        {
            _showableAds = new Dictionary<AdType, List<string>>();
            foreach (Ad ad in Ads)
            {
                if (_showableAds.ContainsKey(ad.Type))
                    _showableAds[ad.Type].Add(ad.PlacementID);
                else
                    _showableAds.Add(ad.Type, new List<string> {ad.PlacementID});
            }
        }

        public override void DeInit()
        {
            _activeAds.SafeForeach((placementId, ad) => { ad.Dispose(); });
            _activeAds = new Dictionary<string, IAd>();
        }

        /// <summary>
        /// show an Interstitial ad, this is a fullscreen ad
        /// </summary>
        public void ShowInterstitialAd()
        {
            Advertisement.Show();
        }

        public BannerAd ShowBannerAd(BannerPosition position, bool force = false)
        {
            string bannedAdId = GetAdPlacementId(AdType.Banner, force);
            if (string.IsNullOrEmpty(bannedAdId))
                return null;

            BannerAd ba = new BannerAd(bannedAdId, position);
            AddEvents(ba);
            _activeAds.Add(bannedAdId, ba);
            ba.Show();
            return ba;
        }

        public BannerAd ShowBannerAd(string placementId, BannerPosition position, bool force = false)
        {
            if (_activeAds.ContainsKey(placementId))
            {
                BannerAd existingAd = _activeAds[placementId] as BannerAd;
                existingAd.Hide();
            }

            BannerAd ba = new BannerAd(placementId, position);
            AddEvents(ba);
            _activeAds.Add(placementId, ba);
            ba.Show();
            return ba;
        }

        public void HideBannerAd(string banner)
        {
            RemoveAd(banner);
        }

        public PersonalizedAd ShowPersonalizedAd(bool force)
        {
            string personalizedAdId = GetAdPlacementId(AdType.PersonalizedPlacement, force);
            if (string.IsNullOrEmpty(personalizedAdId))
                return null;
            RemoveAd(personalizedAdId);

            PersonalizedAd pa = new PersonalizedAd(personalizedAdId);
            AddEvents(pa);
            _activeAds.Add(personalizedAdId, pa);
            return pa;
        }

        private void RemoveAd(string personalizedAdId)
        {
            if (_activeAds.ContainsKey(personalizedAdId))
            {
                IAd existingAd = _activeAds[personalizedAdId];
                existingAd.Dispose();
            }
        }

        public RewardedAd ShowRewardedAd(bool force)
        {
            string rewardedAdId = GetAdPlacementId(AdType.RewardedAd, force);
            if (string.IsNullOrEmpty(rewardedAdId))
                return null;
            RemoveAd(rewardedAdId);

            RewardedAd ra = new RewardedAd(rewardedAdId);
            AddEvents(ra);
            _activeAds.Add(rewardedAdId, ra);
            ra.Show();
            return ra;
        }

        public NonRewardedAd ShowNonRewardedAd(bool force)
        {
            string nonRewardedAd = GetAdPlacementId(AdType.NonRewardedAd, force);
            if (string.IsNullOrEmpty(nonRewardedAd))
                return null;
            RemoveAd(nonRewardedAd);
            NonRewardedAd nra = new NonRewardedAd(nonRewardedAd);
            AddEvents(nra);
            _activeAds.Add(nonRewardedAd, nra);
            nra.Show();
            return nra;
        }

        private void AddEvents(IAd ad)
        {
            ad.Failed += AdOnFailed;
            ad.Finished += AdOnFinished;
            ad.Skipped += AdOnSkipped;
            ad.Disposed += AdOnDisposed;
        }

        private void RemoveEvents(IAd ad)
        {
            ad.Failed -= AdOnFailed;
            ad.Finished -= AdOnFinished;
            ad.Skipped -= AdOnSkipped;
            ad.Disposed -= AdOnDisposed;
        }

        private string GetAdPlacementId(AdType type, bool force = false)
        {
            string id = null;
            if (!_showableAds.ContainsKey(type))
                return id;

            foreach (string placementId in _showableAds[type])
            {
                if (!_activeAds.ContainsKey(placementId))
                    id = placementId;
            }

            if (force)
            {
                var activeAd = _activeAds.FirstOrDefault(ad => IsAppOfType(type, ad.Value));
                if (string.IsNullOrEmpty(activeAd.Key))
                    return id;
                _activeAds.Remove(activeAd.Key);
                RemoveEvents(activeAd.Value);
                activeAd.Value.Dispose();
                id = activeAd.Key;
            }

            return id;
        }

        private bool IsAppOfType(AdType type, IAd ad)
        {
            switch (ad)
            {
                case BannerAd ba:
                    return type == AdType.Banner;
                case PersonalizedAd pa:
                    return type == AdType.PersonalizedPlacement;
                case RewardedAd ra:
                    return type == AdType.RewardedAd;
            }

            return false;
        }

        private void DeactivateAd(IAd ad)
        {
            if (!_activeAds.ContainsKey(ad.PlacementId))
                return;

            _activeAds.Remove(ad.PlacementId);
            RemoveEvents(ad);
            ad.Dispose();
        }

        public override void ApplicationQuit()
        {
        }

        public override void SetMain(Main main)
        {
        }


        private void AdOnSkipped(IAd ad)
        {
            DeactivateAd(ad);
        }

        private void AdOnFinished(IAd ad)
        {
            DeactivateAd(ad);
        }

        private void AdOnFailed(IAd ad)
        {
            DeactivateAd(ad);
        }

        private void AdOnDisposed(IAd ad)
        {
            DeactivateAd(ad);
        }
    }
}