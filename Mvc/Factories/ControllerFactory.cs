using System;

namespace HamerSoft.UnityMvc
{
    public interface ControllerFactory : IDisposable
    {
        Controller GetControllerByView<T>() where T : View;
        Controller GetControllerByView(View view);
        void UnRegisterViews();
    }
}