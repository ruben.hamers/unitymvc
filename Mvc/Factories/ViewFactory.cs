using System;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core;
using UnityEngine;

namespace HamerSoft.UnityMvc
{
    [CreateAssetMenu(fileName = "ViewFactory", menuName = "HamerSoft/MVC/ViewFactory Create", order = 0)]
    public class ViewFactory : RuntimeScriptable
    {
        [SerializeField] private List<BaseView> views;
        protected Dictionary<Type, List<BaseView>> SpanwableViews;

        public override void Init()
        {
            SpanwableViews = new Dictionary<Type, List<BaseView>>();
            foreach (var view in views.Sanitize())
            {
                if (!SpanwableViews.ContainsKey(view.GetType()))
                    SpanwableViews.Add(view.GetType(), new List<BaseView>());
                SpanwableViews[view.GetType()].Add(view);
            }
        }

        public override void DeInit()
        {
        }

        public override void ApplicationQuit()
        {
            SpanwableViews = null;
        }

        public T GetSpawnableView<T>() where T : View
        {
            if (SpanwableViews.ContainsKey(typeof(T)))
                return (T) (object) SpanwableViews[typeof(T)].FirstOrDefault();
            return default;
        }

        public T GetSpawnableView<T>(Func<List<View>, T> filter) where T : View
        {
            if (filter == null)
                return GetSpawnableView<T>();
            if (SpanwableViews.ContainsKey(typeof(T)))
                return filter(SpanwableViews[typeof(T)].Cast<View>().ToList());
            return default;
        }
    }
}