using System;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core;
using UnityEngine;

namespace HamerSoft.UnityMvc
{
    public class DefaultControllerFactory : AssemblyCrawledFactory<Controller>, ControllerFactory
    {
        protected readonly Main Main;

        public DefaultControllerFactory(Main main)
        {
            Main = main;
            CrawlAssembly();
        }

        protected override Controller CreateFactoryObject(Controller value)
        {
            value.SetMain(Main);
            return value;
        }

        public Controller GetControllerByView<T>() where T : View
        {
            return GetControllerByViewType(typeof(T));
        }

        public Controller GetControllerByView(View view)
        {
            return GetControllerByViewType(view.GetType());
        }

        public void UnRegisterViews()
        {
            FactoryObjects.ToList().ForEach(fo=>fo.Value.UnRegisterViews());
        }


        protected Controller GetControllerByViewType(Type type)
        {
            Controller c = null;
            foreach (KeyValuePair<Type, Controller> factoryObject in FactoryObjects)
            {
                if (factoryObject.Key.BaseType != null && !factoryObject.Key.BaseType.GenericTypeArguments.Any(type.IsEqualOrSubClassOrAssingableFrom))
                    continue;
                c = factoryObject.Value;
                break;
            }

            return c;
        }

        protected override Type GetTypeFilter => typeof(BaseController<>);

        public override void Dispose()
        {
            base.Dispose();
            
        }
    }
}