using System;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace HamerSoft.UnityMvc
{
    /// <summary>
    /// In the MVC framework, there will only be 1 controller for n-amount of views. Controllers are not coupled to specific instances but are reusable for all views of that type.
    /// </summary>
    /// <typeparam name="TView">view type</typeparam>
    public abstract class BaseController<TView> : Controller where TView : View
    {
        protected Dictionary<string, TView> Views;
        protected ViewFactory ViewFactory { get; private set; }
        protected Main Main { get; private set; }

        public BaseController()
        {
            Views = new Dictionary<string, TView>();
            RegisterViews();
        }

        private void AssignEvents(TView view)
        {
            if (HasView(view))
                return;
            view.Awoken += OnAwake;
            view.Started += OnStarted;
            view.Enabled += OnEnabled;
            view.Disabled += OnDisabled;
            view.Destroyed += OnDestroyed;
            AssignViewEvents(view);
            Views.Add(view.Id, view);
        }

        private void UnassignEvents(TView view)
        {
            if (!HasView(view))
                return;
            view.Awoken -= OnAwake;
            view.Started -= OnStarted;
            view.Enabled -= OnEnabled;
            view.Disabled -= OnDisabled;
            view.Destroyed -= OnDestroyed;
            UnAssignViewEvents(view);
            Views.Remove(view.Id);
        }

        protected bool HasView(View view)
        {
            return view is TView && Views.ContainsKey(view.Id);
        }

        protected abstract void AssignViewEvents(TView view);
        protected abstract void UnAssignViewEvents(TView view);

        public void RegisterView(View view)
        {
            if (view is TView tView)
                AssignEvents(tView);
        }

        public void RegisterViews()
        {
            var views = GetViews<TView>();
            views?.ToList().ForEach(kvp => AssignEvents(kvp.Value));
        }

        public virtual void UnRegisterViews()
        {
            Views?.ToList().ForEach(kvp => UnassignEvents(kvp.Value));
        }


        protected virtual void OnDisabled(View view, object[] args)
        {
        }

        protected virtual void OnEnabled(View view, object[] args)
        {
        }

        protected virtual void OnStarted(View view, object[] outputArguments)
        {
            view.Started -= OnStarted;
        }

        protected virtual void OnAwake(View view, object[] args)
        {
            view.Awoken -= OnAwake;
        }

        protected virtual void OnDestroyed(View view, object[] args)
        {
            UnassignEvents(CastTo(view));
        }

        /// <summary>
        /// utility method to cast the view.
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        protected TView CastTo(View view)
        {
            if (view is TView casted)
                return casted;

            string message =
                $"View {view}:{view.GetType()} cannot be casted to targetView type {typeof(TView)} in controller {GetType()}!";
            Debug.LogWarning(message);
            throw new InvalidCastException(message);
        }

        public virtual TView SpawnView(Transform parent, Func<List<View>, TView> filter = null)
        {
            TView prefab = ViewFactory.GetSpawnableView<TView>(filter);
            TView view = default;
            if (prefab != null)
            {
                Component component = Object.Instantiate(prefab as Component, parent);
                view = component.GetComponent<TView>();
                AssignEvents(view);
                RegisterChildViews(component);
            }


            return view;
        }

        public virtual TView SpawnView(Transform parent, Func<List<View>, TView> filter = null,
            params object[] initArguments)
        {
            TView view = SpawnView(parent, filter);
            if (view != null)
                view.Init(initArguments);
            return view;
        }

        private void RegisterChildViews(Component component)
        {
            List<View> childViews = component.gameObject.GetComponentsInChildren<View>().ToList();
            try
            {
                childViews.ForEach(cv => Main.GetPlugin<MvcPlugin>().GetControllerByView(cv)?.RegisterView(cv));
            }
            catch (Exception e)
            {
                Debug.LogError($"Caught exception trying to register child views with exception {e}");
            }
        }

        public virtual T SpawnCustomView<T>(Transform parent, Func<List<View>, T> filter = null) where T : View
        {
            try
            {
                return Main.GetPlugin<MvcPlugin>().GetControllerByView<T>().SpawnView(parent, filter);
            }
            catch (Exception e)
            {
                Debug.LogWarning(
                    $"Caught Exception in SpawnCustomView for Controller{GetType()} and viewType {typeof(T)}! with Exception {e}");
                return default;
            }
        }

        public virtual T SpawnCustomView<T>(Transform parent, Func<List<View>, T> filter = null,
            params object[] initializeArguments) where T : View
        {
            T view = SpawnCustomView(parent, filter);
            if (view != null)
                view.Init(initializeArguments);

            return view;
        }

        protected bool IsValid(TView view)
        {
            try
            {
                var valid = (view as Component)?.name;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public void DestroyView(TView target)
        {
            Component c = target as Component;

            if (!c)
                return;
            if (HasView(target))
                Views.Remove(target.Id);

            if (Application.isPlaying)
                Object.Destroy(c.gameObject);
            else
                Object.DestroyImmediate(c.gameObject);
        }

        /// <summary>
        /// Get all views of type in the scene
        /// </summary>
        /// <typeparam name="T">type of view</typeparam>
        /// <returns>views</returns>
        private Dictionary<string, T> GetViews<T>() where T : TView
        {
            return SceneManager.GetActiveScene().GetRootGameObjects()
                .SelectMany(GetViews<T>).ToDictionary(key => key.Id, value => value);
        }

        /// <summary>
        /// Get all views of type in the scene
        /// </summary>
        /// <typeparam name="T">type of view</typeparam>
        /// <returns>views</returns>
        private List<T> GetViews<T>(GameObject root) where T : TView
        {
            return root.GetComponentsInChildren<T>().ToList();
        }

        public virtual void SetMain(Main main)
        {
            if (Application.isPlaying && !main)
                Debug.LogError("Trying to set <MAIN> in BaseController but Main is null!");

            Main = main;
            ViewFactory = Main?.GetPlugin<MvcPlugin>()?.GetViewFactory();
        }

        public virtual void Dispose()
        {
            Main = null;
            ViewFactory = null;
            Views?.SafeForeach((id, view) => { OnDestroyed(view, new object[0]); });
            Views = new Dictionary<string, TView>();
        }
    }
}