using Object = HamerSoft.Core.Object;

namespace HamerSoft.UnityMvc
{
    public abstract class BaseView : Object, View
    {
        public string Id => GetInstanceID().ToString();
        protected abstract object[] CustomStartArgs { get; }

        public event ViewEvent Awoken,
            Started,
            Enabled,
            Disabled,
            Destroyed;

        public virtual void Init(params object[] initArguments)
        {
        }


        protected override void Awake()
        {
            base.Awake();
            Awoken?.Invoke(this);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            Enabled?.Invoke(this);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            Disabled?.Invoke(this);
        }

        protected override void Start()
        {
            base.Start();
            Started?.Invoke(this, CustomStartArgs ?? new object[0]);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            Destroyed?.Invoke(this);
        }
    }
}