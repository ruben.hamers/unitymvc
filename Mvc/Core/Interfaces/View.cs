namespace HamerSoft.UnityMvc
{
    /// <summary>
    /// view event delegate to handle all events with output
    /// </summary>
    /// <param name="view">view reference</param>
    /// <param name="outputArguments">arguments</param>
    public delegate void ViewEvent(View view, params object[] outputArguments);
    public interface View
    {
        string Id { get; }
        
        event ViewEvent Awoken;
        event ViewEvent Started;
        event ViewEvent Enabled;
        event ViewEvent Disabled;
        event ViewEvent Destroyed;

        void Init(params object[] initArguments);
    }
}