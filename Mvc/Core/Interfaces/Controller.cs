using System;
using HamerSoft.Core;

namespace HamerSoft.UnityMvc
{
    public interface Controller : IDisposable
    {
        void SetMain(Main main);
        void RegisterView(View view);
        void RegisterViews();
        void UnRegisterViews();
    }
}