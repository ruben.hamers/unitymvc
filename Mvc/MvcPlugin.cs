using System.Collections.Generic;
using System.Linq;
using HamerSoft.Core;
using HamerSoft.UnityMvc;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HamerSoft.UnityMvc
{
    [CreateAssetMenu(fileName = "MVCPlugin", menuName = "HamerSoft/MVC/MVCPlugin Create", order = 0)]
    public class MvcPlugin : Plugin
    {
        [SerializeField]
        private ViewFactory viewFactory;
        protected ControllerFactory ControllerFactory;

        public override void Init()
        {
            viewFactory.Init();
        }

        public override void DeInit()
        {
            viewFactory.DeInit();
            ControllerFactory.UnRegisterViews();
            ControllerFactory.Dispose();
        }

        public override void ApplicationQuit()
        {
           viewFactory.ApplicationQuit();
        }

        public override void SetMain(Main main)
        {
            ControllerFactory = new DefaultControllerFactory(main);
        }

        public ViewFactory GetViewFactory()
        {
            return viewFactory;
        }

        public BaseController<T> GetControllerByView<T>() where T : View
        {
            return (BaseController<T>)ControllerFactory.GetControllerByView<T>();
        }
        
        public Controller GetControllerByView(View view)
        {
            return ControllerFactory.GetControllerByView(view);
        }
        
    }
}