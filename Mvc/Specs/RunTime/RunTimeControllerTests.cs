using System.Collections;
using HamerSoft.Core;
using HamerSoft.UnityMvc.Specs.RunTime.Mocks;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Object = UnityEngine.Object;

namespace HamerSoft.UnityMvc.Specs.RunTime
{
    public class RunTimeControllerTests
    {
        private MockController mc;
        private MockView mv;
        private Main main;

        [SetUp]
        public void Setup()
        {
            main = Main.Load();
            main.Init();
            mv = new GameObject("MockView").AddComponent<MockView>();
            mc = new MockController();
            mc.SetMain(main);
        }
        
        [UnityTest]
        public IEnumerator When_View_Is_Started_OnStarted_Event_Is_Called()
        {
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(true, mc.StartedCalled);
        }
        [UnityTest]
        public IEnumerator When_View_Is_Disabled_OnDisabled_Event_Is_Called()
        {
            mv.gameObject.SetActive(false);
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(true, mc.OnDisableCalled);
        }

        [UnityTest]
        public IEnumerator When_View_Is_Enabled_OnEnabled_Event_Is_Called()
        {
            mv.gameObject.SetActive(false);
            yield return new WaitForEndOfFrame();
            mv.gameObject.SetActive(true);
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(true, mc.OnEnableCalled);
        }
        [UnityTest]
        public IEnumerator When_View_Is_Destroyed_OnDestroyed_Event_Is_Called()
        {
            mc.DestroyView(mv);
            yield return new WaitForEndOfFrame();
            Assert.AreEqual(true, mc.OnDestroyCalled);
        }

        [TearDown]
        public void Teardown()
        {
            if (mv)
                Object.DestroyImmediate(mv.gameObject);

            mc.Dispose();
            mc = null;
        }
    }
}