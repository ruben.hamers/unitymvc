using System.Collections.Generic;

namespace HamerSoft.UnityMvc.Specs.RunTime.Mocks
{
    public class MockView : BaseView
    {
        protected override object[] CustomStartArgs { get; }
        public object[] InitArgs;
        public override void Init(params object[] initArguments)
        {
            base.Init(initArguments);
            InitArgs = initArguments;
        }
    }

    public class MockController : BaseController<MockView>
    {
        public bool AwakeCalled, StartedCalled, OnEnableCalled, OnDisableCalled, OnDestroyCalled, CustomEventCalled;
        public Dictionary<string,MockView> InternalViews => Views;

        protected override void AssignViewEvents(MockView view)
        {
        }

        protected override void UnAssignViewEvents(MockView view)
        {
            
        }


        protected override void OnAwake(View view, object[] args)
        {
            AwakeCalled = true;
            base.OnAwake(view, args);
        }

        protected override void OnStarted(View view, object[] outputArguments)
        {
            StartedCalled = true;
            base.OnStarted(view, outputArguments);
        }

        protected override void OnDisabled(View view, object[] args)
        {
            OnDisableCalled = true;
            base.OnDisabled(view, args);
        }

        protected override void OnEnabled(View view, object[] args)
        {
            OnEnableCalled = true;
            base.OnEnabled(view, args);
        }

        protected override void OnDestroyed(View view, object[] args)
        {
            OnDestroyCalled = true;
            base.OnDestroyed(view, args);
        }

        public MockView CastView(View view)
        {
            return CastTo(view);
        }
    }
}