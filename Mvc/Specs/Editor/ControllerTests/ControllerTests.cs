using System;
using HamerSoft.Core;
using HamerSoft.UnityMvc.Specs.RunTime.Mocks;
using NUnit.Framework;
using UnityEngine;
using Object = UnityEngine.Object;

namespace HamerSoft.UnityMvc.Specs
{
    public class ControllerTests
    {
        public class TestView : BaseView
        {
            protected override object[] CustomStartArgs { get; }
        }

        private MockController mc;
        private MockView mv;
        private Main main;

        [SetUp]
        public void Setup()
        {
            main = Main.Load();
            main.Init();
            mv = new GameObject("MockView").AddComponent<MockView>();
            mc = new MockController();
            mc.SetMain(main);
        }

        [Test]
        public void Upon_Controller_SetMain_Views_Are_Queried()
        {
            Assert.True(mc.InternalViews.Count > 0);
        }

        [Test]
        public void CastTo_Returns_View_Of_Type_In_Generic_Type_Param()
        {
            Assert.AreEqual(mv, mc.CastView(mv));
        }

        [Test]
        public void CastTo_Throws_Exception_When_View_Is_Incorrect_Type_To_Type_Param()
        {
            TestView tv = new GameObject("TestView").AddComponent<TestView>();
            Assert.Throws<InvalidCastException>(() => { mc.CastView(tv); });
            Object.DestroyImmediate(tv.gameObject);
        }

        [Test]
        public void Controller_Spawns_No_View_When_It_Is_Not_Found()
        {
            var view = mc.SpawnView(null);
            Assert.AreEqual(null, view);
        }

        [Test]
        public void Destroy_View_Removes_View_From_Scene()
        {
            mc.DestroyView(mv);
            Assert.Catch(() =>
            {
                var go = mv.gameObject;
            });
        }

        [Test]
        public void SpawnCustomView_Returns_Null_When_No_View_Is_Found()
        {
            var view = mc.SpawnCustomView<MockView>(null);
            Assert.AreEqual(null, view);
        }

        [TearDown]
        public void Teardown()
        {
            if (mv)
                Object.DestroyImmediate(mv.gameObject);

            mc.Dispose();
            mc = null;
        }
    }
}