using System;
using System.Collections.Generic;
using HamerSoft.Core;
using HamerSoft.UnityMvc;

namespace HamerSoft.UnityMvc.Specs.Mocks
{
    public class MockControllerFactory :DefaultControllerFactory
    {
        public Dictionary<Type, Controller> GetControllers()
        {
            return FactoryObjects;
        }

        public MockControllerFactory(Main main) : base(main)
        {
        }

        public void AddController(Controller c)
        {
            FactoryObjects.Add(c.GetType(), c);
        }
    }
}