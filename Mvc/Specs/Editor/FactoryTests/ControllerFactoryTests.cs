using System.Linq;
using HamerSoft.Core;
using HamerSoft.UnityMvc.Specs.Mocks;
using HamerSoft.UnityMvc.Specs.RunTime.Mocks;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.UnityMvc.Specs
{
    public class ControllerFactoryTests
    {
        private MockControllerFactory _mockControllerFactory;

        private class DerivedMockView : MockView
        {
            
        }

        [SetUp]
        public void Setup()
        {
            var main = Main.Load();
            main.Init();
            _mockControllerFactory = new MockControllerFactory(main);
            _mockControllerFactory.AddController(new MockController());
        }

        [Test]
        public void When_ControllerFactory_Is_Created_ConcreteController_Are_Instantiated()
        {
            Assert.True(_mockControllerFactory.GetControllers().ToList().All(p => !p.Key.IsAbstract));
        }
        [Test]
        public void Get_Controller_By_View_Returns_Correct_Controller()
        {
            Assert.NotNull(_mockControllerFactory.GetControllerByView<MockView>());
            Assert.AreEqual(typeof(MockController),
                _mockControllerFactory.GetControllerByView<MockView>().GetType());
        }
        
        [Test]
        public void Get_Controller_By_View_Returns_Correct_Controller_When_View_Is_Derived()
        {
            Assert.NotNull(_mockControllerFactory.GetControllerByView<DerivedMockView>());
            Assert.AreEqual(typeof(MockController),
                _mockControllerFactory.GetControllerByView<DerivedMockView>().GetType());
        }

        [TearDown]
        public void TearDown()
        {
            _mockControllerFactory.Dispose();
        }
    }
}