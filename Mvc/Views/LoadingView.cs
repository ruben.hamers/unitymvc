using UnityEngine;

namespace HamerSoft.UnityMvc.Views
{
    public class LoadingView : BaseView
    {
        public Transform Loader => loader;
        [SerializeField] private Transform loader;
        protected override object[] CustomStartArgs { get; }
    }
}