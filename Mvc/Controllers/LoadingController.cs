using HamerSoft.Core.Services;
using HamerSoft.UnityMvc.Views;
using UnityEngine;

namespace HamerSoft.UnityMvc.Controllers
{
    public class LoadingController : BaseController<LoadingView>
    {
        protected override void AssignViewEvents(LoadingView view)
        {
        }

        protected override void UnAssignViewEvents(LoadingView view)
        {
            
        }

        protected override void OnStarted(View view, object[] outputArguments)
        {
            base.OnStarted(view, outputArguments);

            var loaderView = CastTo(view);

            void RotateLoader()
            {
                loaderView.Loader.Rotate(new Vector3(0, 0, -5));
            }

            if (loaderView.Loader != null)
                ServiceProvider.GetService<UpdateService>().StartUpdate(RotateLoader);
        }
    }
}